#! /usr/bin/env bash
xcodebuild \
  -scheme Fahrschule\ enterprise \
  -workspace Fahrschule.xcworkspace \
  -sdk iphonesimulator \
  test | xcpretty -s --report junit --output "test-reports/junit-report.xml" || true