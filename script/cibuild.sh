#! /usr/bin/env bash

set -e

ARCHIVEPATH=`pwd`/archive
IPA_NAME=Fahrschule_enterprise
SCHEME=Fahrschule\ enterprise
WORKSPACE_FILE=Fahrschule.xcworkspace
PROJECT_BUILDDIR=${ARCHIVEPATH}/${IPA_NAME}.xcarchive/Products/Applications

printf "\n=== 1. Clearing Output folder === "
rm -rf "${ARCHIVEPATH}"
printf "✅"

printf "\n\n=== 2. Build App ===\n\n"

xcodebuild \
  -scheme "${SCHEME}" \
  -configuration Debug \
  -workspace "${WORKSPACE_FILE}" \
  archive -archivePath "${ARCHIVEPATH}/${IPA_NAME}" \
  CODE_SIGN_IDENTITY="iPhone Distribution: freenet AG" | xcpretty -s

echo "Created App at ${ARCHIVEPATH}/${IPA_NAME}"

# create dsym zip
pushd "${ARCHIVEPATH}/${IPA_NAME}.xcarchive/dSYMs/" >/dev/null
printf "\n=== 3. Packaging dSYMs For Hockey Upload === "
zip -r "${IPA_NAME}.app.dSYM.zip" "${IPA_NAME}.app.dSYM" >/dev/null
popd >/dev/null
mv "${ARCHIVEPATH}/${IPA_NAME}.xcarchive/dSYMs/${IPA_NAME}.app.dSYM.zip" ./archive 
printf "✅\n\n"
echo "Created dSYM.zip at ${ARCHIVEPATH}/${IPA_NAME}.app.dSYM.zip"

printf "\n=== 4. Creating IPA === "
#package
xcrun \
  -sdk iphoneos  \
  PackageApplication \
  -v  "${PROJECT_BUILDDIR}"/*.app \
  -o "${ARCHIVEPATH}/${IPA_NAME}.ipa" > /dev/null
printf "✅\n\n"
printf "Output at ${ARCHIVEPATH}/${IPA_NAME}.ipa\n\n"
echo "=== All done 😬==="