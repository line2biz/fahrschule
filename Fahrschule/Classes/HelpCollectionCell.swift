//
//  HelpCollectionCell.swift
//  Fahrschule
//
//  Created on 15.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

class HelpCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var overlayView: OverlayView!
    
    var helpLabel = UILabel()
    
    var selectedHelpIndicatorView: HelpIndicatorView?
    
    var overlays: [[String: AnyObject]]! { didSet{ self.configureView() }}
    
    func configureView() {
        // Remove old items
        for view in overlayView.subviews {
            if view is OverlayItem {
                view.removeFromSuperview()
            }
        }
        
        for element in overlays {
            let x = element["x"] as? CGFloat
            let y = element["y"] as? CGFloat
            
            let item = OverlayItem()
            item.center = CGPointMake(x!, y!)
            item.text = element["text"] as? String
            overlayView.addSubview(item)
        }
        
        overlayView.holeRect = CGRectZero
    }
    
}
