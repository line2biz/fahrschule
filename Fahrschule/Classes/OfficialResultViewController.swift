//
//  OfficialResultViewController.swift
//  Fahrschule
//
//  Created on 24.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

class OfficialResultViewController: UIViewController {
//        MARK: - Types
    struct MainStoryboard {
        struct ViewControllerIdentifiers {
            static let ExamResults = "ExamResultViewController"
            static let Questions = "OfficialQuestionsController"
        }
    }
    
//    MARK: - Outlets
    @IBOutlet weak var mascotFeedbackLabel: UILabel!
    @IBOutlet weak var numberOfFaultsLabel: UILabel!

//    MARK: Public properties
    var managedObjectContext: NSManagedObjectContext!
    var questionModels: [QuestionModel]!
    var timeLeft: Int = 0
    var numQuestionsNotCorrectAnswered: Int = 0
    
//    MARK: Private properties
    private var maxPoints: Int = 0
    private var mainGroupPoints: Int = 0
    private var points: Int = 0
    private var mainGroupDict = NSMutableDictionary()
    private var additionalGroupDict = NSMutableDictionary()
    private var sortedGroupArray = NSMutableArray()
    
    private let settings = Settings.sharedSettings() as! Settings
    
//    MARK: Constants
    private let incorrectColor = UIColor.colorFromHex(0x9d0300, alpha: 1)
    private let correctColor = UIColor.colorFromHex(0x006600, alpha: 1)
    private let headerHeight: CGFloat = 30.0
    private let buttonFrame = CGRectMake(0, 0, 43, 40)
    private let buttonMargin: CGFloat = 5
    
    
//    MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("ExamSheet", ofType: "plist")!
        let examSheetDictionary = NSDictionary(contentsOfFile: path)
        let keyPath = "\(settings.licenseClass.rawValue).\(settings.teachingType.rawValue).MaxPoints"
        maxPoints = examSheetDictionary!.valueForKeyPath(keyPath)!.integerValue
        
        
        // Statistic
        let examStat = ExamStatistic.insertNewStatistics(managedObjectContext, state: .FinishedExam)
        if let stat = examStat {
            stat.index = 0
            stat.timeLeft = timeLeft
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationUpdateBadgeValue, object: nil)
        
        
        
        var numFivePointsQuestionFalse: Int = 0
        
        
        
        
        for model in self.questionModels {
            
            if let stat = examStat {
                examStat.addStatisticsWithModel(model, inManagedObjectContext: managedObjectContext)
            }
            
            let keyName = model.question.containedIn.containedIn.name
            
            if model.hasAnsweredCorrectly() == false {
                numQuestionsNotCorrectAnswered++
                points += model.question.points.integerValue
                if model.question.points.integerValue == 5 {
                    numFivePointsQuestionFalse++
                }
            }
            
            if model.question.containedIn.containedIn.baseMaterial.boolValue == true {
                
                if model.hasAnsweredCorrectly() == false {
                    mainGroupPoints += model.question.points.integerValue
                }
                
                if let array = mainGroupDict.valueForKey(keyName) as? NSMutableArray {
                    array.addObject(model)
                } else {
                    let array = NSMutableArray()
                    array.addObject(model)
                    mainGroupDict.setObject(array, forKey: keyName)
                    sortedGroupArray.addObject(keyName)
                    
                }
                
            } else {
                if let array = additionalGroupDict.valueForKey(keyName) as? NSMutableArray {
                    array.addObject(model)
                } else {
                    let array = NSMutableArray()
                    array.addObject(model)
                    additionalGroupDict.setObject(array, forKey: keyName)
                    sortedGroupArray.addObject(keyName)
                }
                
            }
            
        }
        
        SNAppDelegate.sharedDelegate().saveContext()
        
        
        
        numberOfFaultsLabel.text = String.localizedStringWithFormat(NSLocalizedString("Klasse %@ - %d Fehlerpunkte", comment: ""), settings.licenseClassString, points)
        
        
        
        if (points > maxPoints || (numFivePointsQuestionFalse == 2 && points == 10 && maxPoints == 10)) {
            self.mascotFeedbackLabel.text = NSLocalizedString("nicht bestanden", comment: "");
            self.mascotFeedbackLabel.backgroundColor = self.incorrectColor;
        }
        else {
            self.mascotFeedbackLabel.text = NSLocalizedString("bestanden", comment: "");
            self.mascotFeedbackLabel.backgroundColor = self.correctColor;
        }
    }
    
    //    MARK: - Outlet methods
    func didTapButtonQuestion(sender: UIButton) {
        
        // Show history
        let controller = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifiers.Questions) as! OfficialQuestionsController
        controller.managedObjectContext = managedObjectContext
        controller.questionModels = questionModels
        controller.questionSheetType = .History
        controller.currentIndexPath = NSIndexPath(forItem: sender.tag, inSection: 0)
        controller.navigationItem.leftBarButtonItem = nil
        navigationController?.pushViewController(controller, animated: true)
        
    }
    
    

}

// MARK: - Table View Datasource
extension OfficialResultViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return mainGroupDict.count
        default:
            return additionalGroupDict.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! OfficialResultCell
        let idx = indexPath.section == 0 ? indexPath.row : indexPath.row + mainGroupDict.count
        
        let key = sortedGroupArray[idx] as! String
        
        cell.titleLabel?.text = key
        
        var correct = 0
        var index = 0
        
        let dict = indexPath.section == 0 ? mainGroupDict : additionalGroupDict
        
        if let array = dict.valueForKey(key) as? [QuestionModel] {
            var xPos = CGRectGetMidX(cell.bounds)
            let yPos = CGRectGetMidY(cell.bounds) - CGRectGetMidY(buttonFrame)
            for model in array {
                var color = correctColor
                if model.hasAnsweredCorrectly() {
                    correct++
                    color = correctColor
                } else {
                    color = incorrectColor
                }
                
                let button = UIButton(frame: buttonFrame)
                button.frame.origin.x = xPos + buttonMargin
                button.frame.origin.y = yPos
                button.backgroundColor = color
                
                cell.contentView.addSubview(button)
                xPos = CGRectGetMaxX(button.frame)
                
                button.tag = model.index;
                button.addTarget(self, action: Selector("didTapButtonQuestion:"), forControlEvents: .TouchUpInside)
                
                
                
            }
            cell.subtitleLabel?.text = "\(correct)/\(array.count)"
        } else {
            cell.subtitleLabel?.text = "0/0"
        }
        return cell
    }
}

// MARK: - Table View delegate
extension OfficialResultViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        view.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), headerHeight)
        view.backgroundColor = UIColor.clearColor()

        let label =  UILabel()
        label.frame = CGRectInset(view.frame, 4, 0)
        label.backgroundColor = UIColor.colorFromHex(0x003f83, alpha: 1)
        label.textAlignment = .Center
        label.textColor = UIColor.whiteColor()
        
        if section == 0 {
            label.text = String.localizedStringWithFormat(NSLocalizedString("Grundstoff - %zd Fehlerpunkte", comment: ""), mainGroupPoints)
        } else {
            label.text = String.localizedStringWithFormat(NSLocalizedString("Zusatzstoff Klasse %@ - %zd Fehlerpunkte", comment: ""), settings.licenseClassString, points - mainGroupPoints)
        }
        view.addSubview(label)
        return view

    }
}

