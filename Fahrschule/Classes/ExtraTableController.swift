//
//  ExtraTableController.swift
//  Fahrschule
//
//  Created on 02.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ExtraTableController: UITableViewController {
    
//    MARK: - Types
    struct MainStoryboard {
        struct SegueIdentifiers {
            static let showStVO = "showStVO"
            static let showLicenseClass = "showLicenseClass"
            static let showFormulas = "showFormulas"
            static let showBrakingDistance = "showBrakingDistance"
            static let showTrafficSigns = "showTrafficSigns"
            static let showHelp = "ShowHelp"
        }
        
        struct CellIdentifiers {
            static let mainCell = "Cell"
        }
    }
    
    
//    MARK: Public properties
    var dataSource: [[String : String]]!
    var selectedIndexPath: NSIndexPath?
    
//    MARK: - Private properties
    var bannerView: GADBannerView!
    
//    MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("Extras", ofType: "plist")
        self.dataSource = NSArray(contentsOfFile: path!) as! [[String : String]]
        
        
        
        if isIPhone() {
            self.clearsSelectionOnViewWillAppear = true
            navigationItem.leftBarButtonItem = nil
        } else {
            self.clearsSelectionOnViewWillAppear = false
        }
        
        // Google banner
        bannerView = ({
            let banner = GADBannerView()
            banner.frame.size = kGADAdSizeBanner.size
            
            banner.adUnitID = ApplicationGoogleAdsID
            banner.rootViewController = self
            
            let request: GADRequest = GADRequest()
            request.testDevices = [ kGADSimulatorID, "d11daefd6bf3ab4ac67af8a6d8b8b264" ]
            banner.loadRequest(request)
            
            self.view.addSubview(banner)
            
            return banner
        })()
        tableView.contentInset.bottom = CGRectGetHeight(bannerView.frame)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        bannerView.center.x = CGRectGetMidX(tableView.bounds)
        bannerView.center.y = tableView.contentOffset.y + tableView.bounds.size.height - CGRectGetMidY(bannerView.bounds) - 50
    }
}

//    MARK: - Table view data source
extension ExtraTableController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(MainStoryboard.CellIdentifiers.mainCell, forIndexPath: indexPath) 
        let item = dataSource[indexPath.row]
        cell.textLabel!.text = item["title"]
        cell.imageView?.image = UIImage(named: item["image"]!)
        return cell
    }
}

//    MARK: - Table View datasource
extension ExtraTableController {
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch indexPath.row {
        case 0:
            self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showFormulas, sender: self)
        case 1:
            self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showBrakingDistance, sender: self)
        case 2:
            self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showTrafficSigns, sender: self)
        case 3:
            self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showStVO, sender: self)
            break
        default:
            let sURL = Settings.sharedSettings().iTunesLink
            UIApplication.sharedApplication().openURL(NSURL(string: sURL)!)
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}

