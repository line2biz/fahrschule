//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let strNumber = "10.5"
if let myNumber = NSNumberFormatter().numberFromString(strNumber) {
    print("asdf number: \(strNumber)")
    print("Int number: \(myNumber.integerValue )")
    print("Float number: \(myNumber.floatValue)")
} else {
    print("nothing to do")
}

for index in 1...5 {
    print("\(index) times 5 is \(index * 5)")
}


struct QuestionSheetType : OptionSetType, BooleanType {
    private var value: UInt
    init(_ rawValue: UInt) { self.value = rawValue }
    
    // _RawOptionSetType
    init(rawValue: UInt) { self.value = rawValue }
    
    // NilLiteralConvertible
    init(nilLiteral: ()) { self.value = 0}
    
    // RawRepresentable
    var rawValue: UInt { return self.value }
    
    // BooleanType
    var boolValue: Bool { return self.value != 0 }
    
    // BitwiseOperationsType
    static var allZeros: QuestionSheetType { return self.init(0) }
    
    // User defined bit values
    static var Learning: QuestionSheetType   { return self.init(0) }
    static var Exam: QuestionSheetType  { return self.init(1 << 0) }
    static var History: QuestionSheetType   { return self.init(1 << 1) }
}


print("Learning \(QuestionSheetType.Learning.rawValue)")
print("Exam \(QuestionSheetType.Exam.rawValue)")
print("History \(QuestionSheetType.History.rawValue)")

func printType(type: QuestionSheetType)->String {
    switch type {
    case QuestionSheetType.Learning:
        return "Learning"
    case QuestionSheetType.Exam:
        return "Exam"
    case QuestionSheetType.History:
        return "History"
    default:
        return "EMPTY"
    }
}

var val: QuestionSheetType = [.Exam, .History]










