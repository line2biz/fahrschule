//
//  LearningResultViewController.swift
//  Fahrschule
//
//  Created on 17.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

class LearningResultViewController: UIViewController {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var succeedView: CircularCounterView!
    @IBOutlet weak var failedView: CircularCounterView!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var errorButton: UIButton!
    
    @IBOutlet var verticalConstraints: [NSLayoutConstraint]!
    
    
    var numQuestionsNotCorrectAnswered: Int = 0
    
    private var masterNavigationController: UINavigationController?
    private var detailNavigationController: UINavigationController?
    
    
    var managedObjectContext: NSManagedObjectContext!
    var questionModels: [QuestionModel]!
    
//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Setup view
        self.succeedView.title = NSLocalizedString("Richtig", comment: "")
        self.failedView.title = NSLocalizedString("Falsch", comment: "");
        
        self.failedView.countLabel.text = "\(self.numQuestionsNotCorrectAnswered)"
        self.succeedView.countLabel.text = "\(self.questionModels.count - numQuestionsNotCorrectAnswered)"
        
        if self.questionModels.count == 1 {
            self.titleLabel.text = NSLocalizedString("Du hast 1 Frage bearbeitet.", comment: "")
        } else {
            self.titleLabel.text = String.localizedStringWithFormat(NSLocalizedString("Du hast %d Fragen bearbeitet.", comment: ""), self.questionModels.count)
        }
        
        self.errorButton.enabled = self.numQuestionsNotCorrectAnswered > 0
        
        if let splitController = navigationController?.parentViewController as? UISplitViewController {
            if splitController.viewControllers.count > 1 {
                masterNavigationController = splitController.viewControllers.first as? UINavigationController
                detailNavigationController = splitController.viewControllers.last as? UINavigationController
            } else {
                print("Unexpected error: QuestionsTableViewController \(__FUNCTION__)")
            }
        }
        
        // Change interface for ealier models then iPhone 5s
        if CGRectGetHeight(UIScreen.mainScreen().bounds) <= 480 {
            for constraint in verticalConstraints {
                constraint.constant = 50
            }
            
            
        }
            // Change interface for iPhone 5s
        else if CGRectGetHeight(UIScreen.mainScreen().bounds) <= 568 {
            for constraint in verticalConstraints {
                constraint.constant = 60
            }
        }

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.toolbarHidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let navContr = masterNavigationController {
            if let questionsController = masterNavigationController?.topViewController as? QuestionsTableViewController {
                if questionsController.dataSource.count != questionModels.count {
                    questionsController.dataSource = questionModels
                }
                questionsController.tableView.reloadData()
            }
        }
    }
    

//    MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let qsvc = segue.destinationViewController as? QuestionSheetViewController {
            
            var newModels = self.questionModels
            
            // Show only incorrect questions
            if let btn = sender as? UIButton {
                if btn == self.errorButton {
                    var models = [QuestionModel]()
                    var index = 0
                    for model in self.questionModels {
                        if !model.hasAnsweredCorrectly() {
                            let newModel = QuestionModel(question: model.question, atIndex: index++)
                            newModel.givenAnswers = model.givenAnswers
                            models.append(newModel)
                        }
                    }
                    newModels = models
                }
            }
            
            
            qsvc.managedObjectContext = self.managedObjectContext
            qsvc.questionModels = newModels
            qsvc.solutionIsShown = true
            qsvc.navigationItem.leftBarButtonItem = nil
            
            if let questionsController = masterNavigationController?.topViewController as? QuestionsTableViewController {
                questionsController.dataSource = newModels
                questionsController.tableView.reloadData()
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                questionsController.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .Top)
            }
    
            
            
        }
        
    }
    
//    MARK: - Outlet functions
    
    @IBAction func didTapButtonClose(sender: AnyObject) {
        if isIPAD() {
            navigationController?.popToRootViewControllerAnimated(true)
            masterNavigationController?.popToRootViewControllerAnimated(true)
            
        } else {
            self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
        }
    }

}
