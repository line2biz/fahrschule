//
//  QuestionsTableViewController.swift
//  Fahrschule
//
//  Created on 18.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit
import GoogleMobileAds

struct QuestionSheetType : OptionSetType, BooleanType {
    private var value: UInt
    init(_ rawValue: UInt) { self.value = rawValue }
    
    // _RawOptionSetType
    init(rawValue: UInt) { self.value = rawValue }
    
    // NilLiteralConvertible
    init(nilLiteral: ()) { self.value = 0}
    
    // RawRepresentable
    var rawValue: UInt { return self.value }
    
    // BooleanType
    var boolValue: Bool { return self.value != 0 }
    
    // BitwiseOperationsType
    static var allZeros: QuestionSheetType { return self.init(0) }
    
    // User defined bit values
    static var None: QuestionSheetType      { return self.init(0) }
    static var Learning: QuestionSheetType  { return self.init(1 << 0) }
    static var Exam: QuestionSheetType      { return self.init(1 << 1) }
    static var History: QuestionSheetType   { return self.init(1 << 2) }
}


class QuestionsTableViewController: UITableViewController, UISplitViewControllerDelegate {

//    MARK: - Types
    struct MainStoryboard {
        struct SegueIdentifiers {
            static let showTestQuestions = "showTestQuestions"
            static let showExamQuestions = "showExamQuestions"
        }
        
        struct Restoration {
            static let subGroupKey = "SUBGROUP_STORE_KEY"
            static let titleKey = "QTVC_TITLE_KEY"
        }
        
        struct CellIdentifier {
            static let Learning = "Cell"
            static let Exam = "ExamCell"
        }
        
        struct ViewControllers {
            static let QuestionsList = "QuestionsTableViewController"
            static let Question = "QuestionSheetViewController"
        }
    }
    
//    MARK: Outlets
    @IBOutlet var queryButton: UIBarButtonItem!
    
//    MARK: Properties
    var masterNavigationController: UINavigationController?
    var detailNavigationController: UINavigationController?
    var questionSheetType: QuestionSheetType = .Learning {
        didSet {
            self.configureView()
            self.updateTableViewCells()
        }
    }

//    MARK: Public
    var managedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext
    var currentIndexPath: NSIndexPath?
    var dataSource: [QuestionModel]!
    var subGroup: SubGroup! {
        didSet {
            let questions = Question.questionsInRelationsTo(subGroup, inManagedObjectContext: self.managedObjectContext) as [AnyObject]
            self.dataSource = QuestionModel.modelsForQuestions(questions) as? [QuestionModel]
            self.title = self.subGroup.name
        }
    }
    
    var favorites: Bool = false
    
    
    
//    MARK: Privates
    private var localObservers = [AnyObject]()
    private var nomatchesView: UIView!
    private var nomatchesLabel: UILabel!
    
    var bannerView: GADBannerView!
    
    
//    MARK: - Initialization & Delocation
    deinit {
        unregisterObservers()
    }
    
  
//    MARK: - State Save and Preservation
    override func encodeRestorableStateWithCoder(coder: NSCoder) {
        super.encodeRestorableStateWithCoder(coder)
        coder.encodeObject(self.subGroup.objectID.URIRepresentation(), forKey: MainStoryboard.Restoration.subGroupKey)
        coder.encodeObject(self.title, forKey: MainStoryboard.Restoration.titleKey)
    }
    
    override func decodeRestorableStateWithCoder(coder: NSCoder) {
        super.decodeRestorableStateWithCoder(coder)
        
//        Restore Subgroup
        self.managedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext
        
        let objURI = coder.decodeObjectForKey(MainStoryboard.Restoration.subGroupKey) as! NSURL
        let objID: NSManagedObjectID = self.managedObjectContext.persistentStoreCoordinator!.managedObjectIDForURIRepresentation(objURI)!
        self.subGroup = self.managedObjectContext.objectWithID(objID) as! SubGroup
        
//        Restore controller title
        self.title = coder.decodeObjectForKey(MainStoryboard.Restoration.titleKey) as? String
        
    }
    
    
//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Observers
        registerObservers()
        
        // TableView settings
        self.clearsSelectionOnViewWillAppear = true
        self.tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.reloadData()
        
        configureView()
        
        // Split cotroller settings
        if let splitController = navigationController?.parentViewController as? UISplitViewController {
            if splitController.viewControllers.count > 1 {
                masterNavigationController = splitController.viewControllers.first as? UINavigationController
                detailNavigationController = splitController.viewControllers.last as? UINavigationController
            } else {
                print("Unexpected error: QuestionsTableViewController \(__FUNCTION__)")
            }
        }
        else if let masterNav = masterNavigationController { // Current controller as search datasource
            let splitController = masterNav.parentViewController as? UISplitViewController
            detailNavigationController = splitController?.viewControllers.last as? UINavigationController

        }
        
        // 'No result' view
        (nomatchesView, nomatchesLabel) = ({
            let view = UIView(frame: self.tableView.frame)
            view.backgroundColor = UIColor.clearColor()
            
            let label = UILabel()
            label.font = UIFont.helveticaNeueBold(17)
            label.textColor = UIColor.grayFont()
            label.textAlignment = .Center
            label.backgroundColor = UIColor.clearColor()
            label.text = NSLocalizedString("Keine Ergebnisse", comment: "")
            label.sizeToFit()
            view.hidden = true
            view.addSubview(label)
            self.tableView.addSubview(view)
            return (view, label)
            
        })()
        
        
        // Google banner
        bannerView = ({
            let banner = GADBannerView()
            banner.frame.size = kGADAdSizeBanner.size
            
            banner.adUnitID = ApplicationGoogleAdsID
            banner.rootViewController = self
            
            let request: GADRequest = GADRequest()
            request.testDevices = [ kGADSimulatorID, "d11daefd6bf3ab4ac67af8a6d8b8b264" ]
            banner.loadRequest(request)
            
            self.view.addSubview(banner)
            
            return banner
        })()
        tableView.contentInset.bottom = CGRectGetHeight(bannerView.frame)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(true, animated: animated)
        
        self.tableView.reloadData()
        if let path = currentIndexPath {
            tableView.selectRowAtIndexPath(path, animated: false, scrollPosition: UITableViewScrollPosition.Middle)
            currentIndexPath = nil
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        bannerView.center.x = CGRectGetMidX(tableView.bounds)
        bannerView.center.y = tableView.contentOffset.y + tableView.bounds.size.height - CGRectGetMidY(bannerView.bounds)
    }
    
//    MARK: - Observers
    func registerObservers() {
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()
        
        
        // Highlight answered question
        localObservers.append(center.addObserverForName(SettingsNotificationDidChangeAnswersGiven, object: nil, queue: queue, usingBlock: {
            [weak self] note in
            
            if let model = note.object as? QuestionModel {
                
                if let array = self?.dataSource {
                    if let idx = array.indexOf(model ) {
                        let indexPath = NSIndexPath(forRow: idx, inSection: 0)
                        if let cell = self?.tableView.cellForRowAtIndexPath(indexPath) as? QuestionCell {
                            cell.setAnswerGiven(true, animated: true)
                        }
                    }
                }
            }
        }))

        
        // Select current question when current question has been changed in Split detail controller
        if isIPAD() {
            localObservers.append(center.addObserverForName(SettingsNotificationDidSelectQuestion, object: nil, queue: queue, usingBlock: {
                [weak self] note in
                if let indexPath = note.object as? NSIndexPath {
                    if let arr = self?.tableView.indexPathsForVisibleRows as [NSIndexPath]! {
                        if arr.count > 0 {                            
                            var scrollPosition: UITableViewScrollPosition = .None
                            
                            if indexPath.compare(arr.first!) == .OrderedAscending {
                                scrollPosition = .Top
                            }
                            else if indexPath.compare(arr.last!) == .OrderedDescending {
                                scrollPosition = .Bottom
                            }
                            
                            self?.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: scrollPosition)
                        }
                    }
                    
                    
                }
                
                }))
        }
        
        
        
        
    }
    
    func unregisterObservers() {
        let center = NSNotificationCenter.defaultCenter()
        for observer in self.localObservers {
            center.removeObserver(observer)
        }
    }
    
//    MARK: - Private functions
    func didChangeAnswersGiven(note: NSNotification) {
        print("QuestionSheetViewController : \(__FUNCTION__)")
    }
    
    func updateTableViewCells() {
        // There is no necessrity to update cell for first time
        if self.questionSheetType == .Exam || self.questionSheetType == .Learning {
            return
        }
        
        let arr = self.tableView.indexPathsForVisibleRows
        if arr?.count > 0 && arr?.count == dataSource.count {
            tableView.reloadRowsAtIndexPaths(arr!, withRowAnimation: .Automatic)
        } else {
            tableView.reloadData()
        }
        
    }
    
    func configureView() {
        
        if navigationController == masterNavigationController {
            navigationItem.setRightBarButtonItem(nil, animated: true)
            navigationItem.setHidesBackButton(true, animated: true)
        }
    }
    
    
    
/*
    Generates Question Sheet controller
*/
    func questionarieController(forType type: QuestionSheetType, indexPath: NSIndexPath, models: [QuestionModel]?)->QuestionSheetViewController {

        let questionarieController = storyboard?.instantiateViewControllerWithIdentifier("QuestionSheetViewController") as! QuestionSheetViewController
        questionarieController.managedObjectContext = managedObjectContext
        if models == nil {
            questionarieController.questionModels = dataSource
            questionarieController.currentIndexPath = indexPath
        } else {
            questionarieController.questionModels = models
            questionarieController.currentIndexPath = indexPath
        }
        return questionarieController
    }
    
    func showQuestionarieController(title: String, models: [QuestionModel]) {
        return showQuestionarieController(title, models: models, indexPath: NSIndexPath(forItem: 0, inSection: 0))
    }
    
    func showQuestionarieController(title: String, models: [QuestionModel], indexPath: NSIndexPath) {
        
        if models.count > 0 {
            let questionarieController = self.questionarieController(forType: QuestionSheetType.Learning, indexPath: indexPath, models: models)
            questionarieController.currentIndex = indexPath.row
            if self.navigationController == self.masterNavigationController {
                self.dataSource = models
                self.tableView.reloadData()
                self.title = title
                
                self.detailNavigationController?.pushViewController(questionarieController, animated: true)
                
            } else {
                var questionsController = self.masterNavigationController?.topViewController as? QuestionsTableViewController
                if questionsController == nil {
                    questionsController = self.storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllers.QuestionsList) as? QuestionsTableViewController
                    questionsController?.dataSource = models
                    questionsController?.tableView.reloadData()
                    questionsController?.questionSheetType = QuestionSheetType.Learning
                    questionsController?.title = title
                    self.masterNavigationController?.pushViewController(questionsController!, animated: true)
                }
                
                if isIPAD() {
                    self.navigationController?.pushViewController(questionarieController, animated: true)
                } else {
                    let navController = UINavigationController(rootViewController: questionarieController)
                    self.presentViewController(navController, animated: true, completion: nil)
                }
                
            }
        }
    }
    

//    MARK: - Outlet functions
    @IBAction func didTapButtonQuery(sender: AnyObject) {

        let title = NSLocalizedString("Abfragen", comment: "")
        let msg = NSLocalizedString("Was soll abgefragt werden?", comment: "")
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Abbrechen", comment: ""), style: .Cancel, handler: nil))
        
        // All questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle Fragen", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            let questions = Question.questionsInRelationsTo(self.subGroup, inManagedObjectContext: self.managedObjectContext)
            let models = QuestionModel.modelsForQuestions(questions) as! [QuestionModel]
            self.showQuestionarieController(alertAction.title!, models: models)
        }))
        
        
        // Incorrect answered questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle falsch Beantworteten", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            let questions = Question.questionsInRelationsTo(self.subGroup, state: .FaultyAnswered, inManagedObjectContext: self.managedObjectContext)
            let models = QuestionModel.modelsForQuestions(questions) as! [QuestionModel]
            self.showQuestionarieController(alertAction.title!, models: models)
            
        }))
        
        
        // All unanswered
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle Unbeantworteten", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            let questions = Question.questionsInRelationsTo(self.subGroup, state: .StateLess, inManagedObjectContext: self.managedObjectContext)
            let models = QuestionModel.modelsForQuestions(questions) as! [QuestionModel]
            self.showQuestionarieController(alertAction.title!, models: models)
            
        }))
        
        
        // Incorrect answered and unanswered questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Falsch & Unbeantwortete", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            let questions = Question.questionsInRelationsTo(self.subGroup, state: .FaultyAnswered, inManagedObjectContext: self.managedObjectContext)
            var models = QuestionModel.modelsForQuestions(questions) as! [QuestionModel]
            let lessQuestions = Question.questionsInRelationsTo(self.subGroup, state: .StateLess, inManagedObjectContext: self.managedObjectContext)
            models += QuestionModel.modelsForQuestions(lessQuestions) as! [QuestionModel]
            self.showQuestionarieController(alertAction.title!, models: models)
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
}


//    MARK: - Table view data source
extension QuestionsTableViewController {
    //    Keine Ergebnisse
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        println("asdfasdf \(self.dataSource) nomatchesView \(nomatchesView)")
        
        if let arr = self.dataSource {
            if self.dataSource.count == 0 {
                if let view = nomatchesView {
                    view.center = CGPointMake(CGRectGetMidX(self.tableView.bounds), CGRectGetHeight(tableView.bounds) / 3)
                    view.hidden = false
                }
            } else {
                if let view = nomatchesView {
                    view.hidden = true
                }
            }
            
            return self.dataSource.count
            
        } else {
            return 0
        }
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let model = self.dataSource[indexPath.row]
        let question = model.question
        
        var cell: QuestionCell
        
        if self.questionSheetType.intersect(.Learning) {
            cell = tableView.dequeueReusableCellWithIdentifier(MainStoryboard.CellIdentifier.Learning, forIndexPath: indexPath) as! QuestionCell
            cell.numberLabel.text = question.number
            cell.answerGiven = model.isAnAnswerGiven()
            
            if question.learnStats != nil && question.learnStats.count > 0 && (questionSheetType.intersect(.History) || navigationController == detailNavigationController || self.isIPhone()) {
                var set = question.learnStats as NSSet
                let arr = set.allObjects
                for learnStat in arr {
                    let state = learnStat.state as NSNumber
                    switch state.integerValue {
                    case StatisticState.CorrectAnswered.rawValue:
                        cell.iconImageView!.image = UIImage(named: "richtig")
                    case StatisticState.FaultyAnswered.rawValue:
                        cell.iconImageView!.image = UIImage(named: "falsch")
                    default:
                        cell.iconImageView!.image = nil
                    }
                }
            } else {
                cell.iconImageView.image = nil
            }
            
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier(MainStoryboard.CellIdentifier.Exam, forIndexPath: indexPath) as! QuestionCell
            cell.answerGiven = model.isAnAnswerGiven()
            
            if self.questionSheetType.intersect(.History) {
                if model.hasAnsweredCorrectly() {
                    cell.iconImageView.image = UIImage(named: "richtig")
                } else {
                    cell.iconImageView.image = UIImage(named: "falsch")
                }
            } else {
                cell.iconImageView.image = nil
            }
            
            
        }
        
        cell.titleLabel.text = question.text
        
        return cell
        
    }
}


//    MARK: - Table View delegate
extension QuestionsTableViewController {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let model = dataSource[indexPath.row]
        
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Pad where navigationController == nil: // Search
            
            // Master controller
            if masterNavigationController?.topViewController is QuestionsTableViewController {
                
            } else {
                let questionsController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllers.QuestionsList) as! QuestionsTableViewController
                questionsController.managedObjectContext = managedObjectContext
                questionsController.dataSource = dataSource
                masterNavigationController?.pushViewController(questionsController, animated: false)
                //                navigationController?.pushViewController(questionsController, animated: true)
                
            }
            
            // Detail controller
            if let questionarieController = detailNavigationController?.topViewController as? QuestionSheetViewController {
                questionarieController.showQuestionAtIndexPath(indexPath)
            } else {
                let questionarieVC = questionarieController(forType: questionSheetType, indexPath: indexPath, models: nil)
                detailNavigationController?.pushViewController(questionarieVC, animated: true)
            }
            
        case .Pad where favorites == true: // Favorites mode
            
            let controller = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllers.QuestionsList) as! QuestionsTableViewController
            controller.title = NSLocalizedString("Suchergebnisse", comment: "")
            controller.title = NSLocalizedString("Markierte", comment: "")
            controller.managedObjectContext = managedObjectContext
            controller.dataSource = dataSource
            controller.questionSheetType = QuestionSheetType.Learning
            controller.currentIndexPath = indexPath
            
            navigationController?.pushViewController(controller, animated: true)
            
            showQuestionarieController(NSLocalizedString("Markierte", comment: ""), models: dataSource, indexPath: indexPath)
            
            
        case .Pad where masterNavigationController == navigationController: // Controller is shown in Split master controller
            
            
            if let questionarieController = detailNavigationController?.topViewController as? QuestionSheetViewController {
                questionarieController.showQuestionAtIndexPath(indexPath)
            } else {
                let questionarieVC = questionarieController(forType: questionSheetType, indexPath: indexPath, models: nil)
                questionarieVC.solutionIsShown = true
                questionarieVC.navigationItem.leftBarButtonItem = nil
                detailNavigationController?.pushViewController(questionarieVC, animated: true)
            }
            
        case .Pad where detailNavigationController == navigationController: // Controller is shown in Split detail controller
            
            // Update Master Controller
            if masterNavigationController?.topViewController is QuestionsTableViewController {
                
            } else {
                let questionsVC = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllers.QuestionsList) as! QuestionsTableViewController
                questionsVC.managedObjectContext = managedObjectContext
                questionsVC.subGroup = subGroup
                questionsVC.currentIndexPath = tableView.indexPathForSelectedRow
                questionsVC.questionSheetType = .Learning
                
                masterNavigationController?.pushViewController(questionsVC, animated: true)
                
            }
            
            let questionarieVC = questionarieController(forType: questionSheetType, indexPath: indexPath, models: nil)
            navigationController!.pushViewController(questionarieVC, animated: true)
            
        default: // iPhone
            let questionarieVC = questionarieController(forType: questionSheetType, indexPath: indexPath, models: nil)
            let navController = UINavigationController(rootViewController: questionarieVC)
            self.presentViewController(navController, animated: true, completion: nil)
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
}

//   MARK:  - Search Results Updating delegate
extension QuestionsTableViewController: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        // updateSearchResultsForSearchController(_:) is called when the controller is being dismissed to allow those who are using the controller they are search as the results controller a chance to reset their state. No need to update anything if we're being dismissed.
        if !searchController.active {
            return
        }
        
        let searchString = searchController.searchBar.text
        let questions = Question.questionsForSearchString(searchString, inManagedObjectContext: self.managedObjectContext) as? [Question]
        self.dataSource = QuestionModel.modelsForQuestions(questions) as? [QuestionModel]
        self.tableView.reloadData()
        
    }
}

