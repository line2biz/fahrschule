//
//  HelpIndicatorView.swift
//  Fahrschule
//
//  Created on 15.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

protocol HelpIndicatorViewDelegate: NSObjectProtocol {
    func didTouchHelpIndicatorView(helpIndicatorView: HelpIndicatorView)->Void
}

class HelpIndicatorView: UIView {
    
    var isAnimating: Bool = false
    var didGlow: Bool = false
    
    struct  MainStroryboard {
        static let RADIUS: CGFloat = 17.0
    }
    
    
    weak var delegate: HelpIndicatorViewDelegate?
    
    var radius: CGFloat = MainStroryboard.RADIUS
    var text: String? = nil
    var icon: UIImage? = nil
    var selected: Bool = false
    var largeCircleFrame: CGRect = CGRectNull
    
    
//    MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = UIColor.clearColor()
        layer.shadowColor = UIColor.colorFromHex(0xB8D8FC, alpha: 1.0).CGColor
        layer.shadowRadius = 8.0
        layer.shadowOffset = CGSizeZero
        
    }

//    MARK: - Drawing
    override func drawRect(rect: CGRect) {
        
        guard let ctx = UIGraphicsGetCurrentContext() else {
            return
        }
        
        CGContextSetLineWidth(ctx, 2.0);
        
        if radius != MainStroryboard.RADIUS && isAnimating == false {
            
            if let img =  self.icon {
                
                createHelpIndicatorBackgroundUsingRect(rect).drawInRect(rect)
                fillCircle(ctx)
                
                let imageRect = CGRectMake((rect.size.width / 2.0) - self.radius * CGFloat(cos(M_PI_4)), (rect.size.height / 2.0) - self.radius * CGFloat(sin(M_PI_4)), 44.0, 39.0)
                img.drawInRect(imageRect)
                
            }
            else {
                createHelpIndicatorBackgroundUsingRect(rect).maskWithMaskImage(createHelpIndicatorBackgroundUsingRect(rect)).drawInRect(rect)
            }
        }
        
        else if isAnimating == false {
            
            fillCircle(ctx)
            if let img =  self.icon {
                let imageRect = CGRectMake((rect.size.width / 2.0) - self.radius * CGFloat(cos(M_PI_4)), (rect.size.height / 2.0) - self.radius * CGFloat(sin(M_PI_4)), 24.0, 21.0)
                img.drawInRect(imageRect)
            }
            else {
                UIColor.whiteColor().setFill()
                drawString("?", font: UIFont.boldSystemFontOfSize(28), contextRect: rect)
            }
        }
        
        else if let img =  self.icon {
            if isAnimating == true {
                fillCircle(ctx)
                let imgSize = self.selected ? CGSizeMake(44.0, 39.0) : CGSizeMake(24.0, 21.0);
                let imageRect = CGRectMake((rect.size.width / 2.0) - self.radius * CGFloat(cos(M_PI_4)), (rect.size.height / 2.0) - self.radius * CGFloat(sin(M_PI_4)), imgSize.width, imgSize.height)
                img.drawInRect(imageRect)
                
            }
        }
        
        
        UIColor.colorFromHex(0x9CB3DF, alpha: 1).setStroke()
        
        CGContextAddArc(ctx, rect.size.width / 2.0, rect.size.height / 2.0, self.radius, 0.0, 2 * CGFloat(M_PI), 0);
        CGContextStrokePath(ctx);
        
    }

//    MARK: - Touches
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        toggleSelected()
        if (delegate?.respondsToSelector("didTouchHelpIndicatorView:") != nil) {
            delegate!.didTouchHelpIndicatorView(self)
        }
        
    }
    
    
//    MARK: - Private functions
    
    private func createHelpIndicatorBackgroundUsingRect(rect: CGRect)->UIImage {
        
        UIGraphicsBeginImageContext(rect.size);
        let ctx2 = UIGraphicsGetCurrentContext()
        CGContextSetRGBFillColor(ctx2, 0.0, 0.0, 0.0, 0.8);
        CGContextFillRect(ctx2, rect);
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsPopContext();
        
        return image

    }

    private func fillCircle(context: CGContextRef) {
        UIColor.colorFromHex(0x2d3341, alpha: 1).setFill()
        
        CGContextAddArc(context,  self.frame.size.width / 2.0, self.frame.size.height / 2.0, self.radius, 0.0, 2 * CGFloat(M_PI), 0);
        CGContextFillPath(context);
    
    }
    
    private func createHelpIndicatorMaskUsingRect(rect: CGRect)->UIImage {
        UIGraphicsBeginImageContext(rect.size);
        let ctx2 = UIGraphicsGetCurrentContext();
        CGContextSetRGBFillColor(ctx2, 0.0, 0.0, 0.0, 1.0);
        CGContextFillRect(ctx2, rect);
        
        CGContextSetRGBFillColor(ctx2, 1.0, 1.0, 1.0, 1.0);
        CGContextFillEllipseInRect(ctx2, CGRectInset(rect, 1.0, 1.0));
        
        let maskImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsPopContext();
        
        return maskImage
    }
    
    
    private func drawString(s: String, font: UIFont, contextRect: CGRect) {
    
        let fontHeight = font.pointSize
        let yOffset = floor( (CGRectGetHeight(contextRect) - fontHeight) / 2.0 ) - 2.0
        let textRect = CGRectMake(0.0, yOffset, contextRect.size.width, fontHeight);
        
        let paragraphStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        
        paragraphStyle.lineBreakMode = NSLineBreakMode.ByClipping
        paragraphStyle.alignment = .Center
        
        let str = NSAttributedString(string: s, attributes: [ NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle ])
        str.drawInRect(textRect)
    
    }
    
//    MARK: - Public functins
    func toggleSelected() {
    
        self.selected = !self.selected;
        
        UIView.animateWithDuration(0.2, animations: { _ in
            self.isAnimating = true
            if (self.selected) {
                self.radius = 31.0
                let x = self.frame.origin.x - 12.0
                let y = self.frame.origin.y - 12.0
                self.frame = CGRectMake(x, y, 64.0, 64.0)
            }
            else {
                self.radius = MainStroryboard.RADIUS
                let x = self.frame.origin.x + 12.0
                let y = self.frame.origin.y + 12.0
                self.frame = CGRectMake(x, y, 40.0, 40.0)
            }
            
            self.setNeedsDisplay()
            
        }) { finished in
            self.isAnimating = false
            self.setNeedsDisplay()
        }
    }
    
}
