//
//  InquirerTableCell.swift
//  Fahrschule
//
//  Created on 16.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

class InquirerTableCell: UITableViewCell {

    @IBOutlet weak var letterLabel: UILabel!
    @IBOutlet weak var corretImageView: UIImageView!
    @IBOutlet weak var questionLabel: UILabel!
    
}
