//
//  UIImage+Extension.swift
//  Fahrschule
//
//  Created on 15.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func maskWithMaskImage(maskImage: UIImage)->UIImage {
        
        let maskRef = maskImage.CGImage;
        
        let mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
            CGImageGetHeight(maskRef),
            CGImageGetBitsPerComponent(maskRef),
            CGImageGetBitsPerPixel(maskRef),
            CGImageGetBytesPerRow(maskRef),
            CGImageGetDataProvider(maskRef), nil, false);
        
        let masked = CGImageCreateWithMask(self.CGImage, mask)
//        let ret = UIImage(CIImage: masked)
        
        let ret = UIImage(CGImage: masked!)
        
        return ret
    
    }
    
}
