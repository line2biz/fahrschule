//
//  InquirerCollectionCell.swift
//  Fahrschule
//
//  Created on 15.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit


class InquirerCollectionCell: UICollectionViewCell, UITableViewDataSource, UITableViewDelegate {
    
//    MARK: - Outlets
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var videoImageView: VideoImageView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    // Overlay
    @IBOutlet weak var numberOverlay: UIView!
    @IBOutlet weak var prefixLabel: UILabel!
    @IBOutlet weak var numberImageView: UIImageView!
    @IBOutlet weak var numberTextFieldPrefixLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    // Overlay outlets
    @IBOutlet var numberTextFields: [UITextField]!
    @IBOutlet var numberTextFieldLabels: [UILabel]!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // Public properties
    var questionModel: QuestionModel! { didSet { self.configureView() } }
    var managedObjectContext: NSManagedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext
    
    
    // Private properties
    private let letters = ["A", "B", "C"]
    private var answers: [Answer]!
    private var localObservers = [AnyObject]()
    
//    MARK: - View Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
        self.backgroundView = view
        
        numberOverlay.backgroundColor = UIColor.whiteColor()
        
//         Register cell classes
        let nameSpaceClassName = NSStringFromClass(InquirerTableCell.self)
        let className = nameSpaceClassName.componentsSeparatedByString(".").last! as String
        tableView.registerNib(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: "Cell")
        tableView.rowHeight = 44.0
        
        
        
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        tableView.dataSource = nil
        tableView.delegate = nil
        tableView.reloadData();
        
        unregisterObservers()
    }
    
    //    MARK: - Observers
    func registerObservers() {
        
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()
        
        unregisterObservers()
        
        localObservers.append(center.addObserverForName(UIKeyboardWillShowNotification, object: nil, queue: queue, usingBlock: {
            [weak self] note in
            if let strongSelf = self {
                strongSelf.keyboardWillShow(note)
            }
            
            }))
        
        localObservers.append(center.addObserverForName(UIKeyboardWillHideNotification, object: nil, queue: queue, usingBlock: {
            [weak self] note in
            if let strongSelf = self {
                strongSelf.keyboardWillHide(note)
            }
            
            }))
        
    }
    
    func unregisterObservers() {
        let center = NSNotificationCenter.defaultCenter()
        
        for observer in localObservers {
            center.removeObserver(observer)
        }
        
        localObservers.removeAll(keepCapacity: true)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        // Get information about the animation.
        let animationDuration: NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        // Get keyboard frame
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        // It is better to user 'magic numbers'
        bottomConstraint.constant = CGRectGetHeight(keyboardScreenEndFrame) - 44
        
        UIView.animateWithDuration(animationDuration, animations: { () -> Void in
            self.layoutIfNeeded()
        })
        
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        // Get information about the animation.
        let animationDuration: NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        bottomConstraint.constant = 0
        
        UIView.animateWithDuration(animationDuration, animations: { () -> Void in
            self.layoutIfNeeded()
        })
    }

//    MARK: - Outlet functins
    
    @IBAction func didTapButtonFavorites(sender: UIButton) {
        sender.selected = !sender.selected
        questionModel.question.setTagged(sender.selected, inManagedObjectContext: managedObjectContext)
        NSNotificationCenter.defaultCenter().postNotificationName("tagQuestion", object: nil)
    }
    

//    MARK: - Private functions
    func configureView() {
        
        if questionModel.question.prefix != "" {
            questionLabel.text = "\(questionModel.question.text)\n\(questionModel.question.prefix)"
        } else {
            questionLabel.text = questionModel.question.text
        }
        
        favoriteButton.selected = questionModel.question.isQuestionTagged()

        let imageName: String = self.questionModel.question.image
        if imageName.characters.count > 0 {
            if let idx = imageName.rangeOfString(".mp4") {
                    let url = NSBundle.mainBundle().URLForResource("/fahrschule-videos/\(imageName)", withExtension: "")
                    videoImageView.setVideo(url, startImage: nil, endImage: nil)
            } else {
               videoImageView.image = UIImage(named: imageName)
            }
            
        } else {
            videoImageView.image = nil
            videoImageView.showBanner()
        }
        
        
//        let docsPath = NSBundle.mainBundle().resourcePath! + "/fahrschule-videos"
//        
//        if let idx = imageName.rangeOfString(".mp4") {
//            let url = NSBundle.mainBundle().URLForResource("/fahrschule-videos/\(imageName)", withExtension: "")

        
        
        var givenAnswers = questionModel.givenAnswers as [AnyObject]
        questionModel.numGivenAnswers = UInt(givenAnswers.count)
        
        // Number Questions
        if questionModel.question.whatType() == .NumberQuestion {
            
            numberImageView.image = nil
            
            if questionModel.question.number == "11.11.1" { // The one single special case where a question needs two numbers.
                numberTextFields[1].hidden = false
                numberTextFieldLabels[1].hidden = false
                
            }
            
            // Hide table view
            tableView.hidden = true
            numberOverlay.hidden = false
            
            let set = questionModel.question.choices as NSSet
            let answer = set.allObjects[0] as! Answer
            let numberTextField = self.numberTextFields[0]
            numberLabel.text = answer.text
            numberTextField.delegate = self
            
            if questionModel.hasSolutionBeenShown {
                let set = questionModel.question.choices as NSSet
                let answer = set.allObjects[0] as! Answer
                if questionModel.question.answerState(givenAnswers) == .FaultyAnswered {
                    numberImageView.image = UIImage(named: "falsch")
                }
                else {
                    numberImageView.image = UIImage(named: "richtig")
                }
                
                
                let numberTextField = numberTextFields[0];
                self.numberLabel.hidden = false
                numberTextField.enabled = false
                
                if questionModel.givenAnswers.count > 0 &&  givenAnswers[0] is NSNumber {
                    let value = givenAnswers[0] as! NSNumber
                    numberTextField.text = value.integerValue == -1 ? "" : value.stringValue
                }
                
                if numberTextFields[1].hidden == false {
                    let correctNumbers = answer.correctNumber.stringValue.componentsSeparatedByString(".")
                    numberLabel.text = answer.text.stringByReplacingOccurrencesOfString("X", withString: correctNumbers[0]).stringByReplacingOccurrencesOfString("Y", withString: correctNumbers[1])
                    
                    if let value = givenAnswers[1] as? NSNumber {
                        let secondNumberTextField = numberTextFields[1]
                        secondNumberTextField.text = value.intValue == -1 ? "" : value.stringValue
                        secondNumberTextField.enabled = false
                    }
                }
                else {
                    numberLabel.text = answer.text.stringByReplacingOccurrencesOfString("X", withString:answer.correctNumber.stringValue)
                }
                
                
            } else {
                registerObservers()
                
                if givenAnswers.count > 0 && givenAnswers[0] is NSNumber {
                    let value = givenAnswers[0] as! NSNumber
                    numberTextField.text = value.intValue == -1 ? "" : value.stringValue
                }
                
                if givenAnswers.count > 1 && givenAnswers[1] is NSNumber {
                    let secondNumberTextField = numberTextFields[1]
                    let value = givenAnswers[1] as! NSNumber
                    secondNumberTextField.text = value.intValue == -1 ? "" : value.stringValue
                    
                }
                
                
                
                if answer.text.hasPrefix("X ") {
                    numberLabel.hidden = !questionModel.hasSolutionBeenShown
                    numberTextFieldPrefixLabel.text = answer.text.stringByReplacingOccurrencesOfString("X ", withString:"")
                    numberTextFieldPrefixLabel.hidden = false
                }
                else {
                    numberTextFieldPrefixLabel.hidden = true;
                }
                
                // Unofficial should 'X ='
                numberTextFieldLabels[0].text = "Antwort:"
            }
            
            
            
        } else {
            
            // Hide number overlay
            tableView.hidden = false
            numberOverlay.hidden = true
            
            
            
            let choices = self.questionModel.question.rearrangedChoices as NSSet
            self.answers = choices.allObjects as! [Answer]
            
            if tableView.dataSource == nil {
                tableView.dataSource = self
            }
            
            if tableView.delegate == nil {
                tableView.delegate = self;
            }
            self.tableView.reloadData()
            
            //        Select answered questions
            var indexPathes: NSMutableArray = NSMutableArray()
            self.tableView.allowsMultipleSelection = true
            
            for (idx, element) in answers.enumerate() {
                if self.questionModel.givenAnswers.containsObject(element) {
                    let indexPath = NSIndexPath(forRow: idx, inSection: 0)
                    self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
                    let cell = tableView.cellForRowAtIndexPath(indexPath)
                    cell?.highlighted = true
                }
            }
            
            if self.questionModel.hasSolutionBeenShown {
                self.showSolutions()
            }
            
            self.tableView.allowsMultipleSelection = !self.questionModel.hasSolutionBeenShown
            if self.questionModel.hasSolutionBeenShown {
                self.tableView.allowsSelection = false
            }
        }
        
        
    }
    
    func showSolutions() {
        if self.questionModel.question.whatType() == QuestionType.ChoiceQuestion {
            for (idx, answer) in self.answers.enumerate() {
                let indexPath = NSIndexPath(forRow: idx, inSection: 0)
                let cell = self.tableView.cellForRowAtIndexPath(indexPath) as! InquirerTableCell
                cell.corretImageView.image = nil
                if cell.selected == true {
                    let imageName = answer.correct.boolValue ? "richtig" : "falsch"
                    cell.corretImageView.image = UIImage(named: imageName)
                } else {
                    if answer.correct.boolValue {
                        cell.corretImageView.image = UIImage(named: "richtig")
                    } else {
                        cell.corretImageView.image = nil
                    }
                }
                
            }
        } else {
            
            let set = questionModel.question.choices as NSSet
            let answer = set.allObjects[0] as! Answer
            let givenAnswers = questionModel.givenAnswers as [AnyObject]
            
            if questionModel.question.answerState(givenAnswers) == .FaultyAnswered {
                numberImageView.image = UIImage(named: "falsch")
            } else {
                numberImageView.image = UIImage(named: "richtig")
            }
            
            let numberTextField = numberTextFields.first!
            numberLabel.hidden = false
            numberTextField.enabled = false
            
            if givenAnswers.count > 0 {
                if let value = givenAnswers.first as? NSNumber {
                    numberTextField.text = value.integerValue == -1 ? "" : value.stringValue
                }
            }
            
            if numberTextFields[1].hidden == false {
                let correctNumbers: [String] = answer.correctNumber.stringValue.componentsSeparatedByString(".")
                
                
                let text = answer.text.stringByReplacingOccurrencesOfString("X", withString: correctNumbers.first!)
                numberLabel.text = text.stringByReplacingOccurrencesOfString("Y", withString: correctNumbers.first!)
                
                if let value = givenAnswers.first as? NSNumber {
                    let secondNumberTextField = numberTextFields[1]
                    secondNumberTextField.text = value.integerValue == -1 ? "" : value.stringValue
                    secondNumberTextField.enabled = false
                }
                
                
            } else {
                numberLabel.text = answer.text.stringByReplacingOccurrencesOfString("X", withString: answer.correctNumber.stringValue)
                
            }
            
        }
        
    }
    
//     MARK: - Table View Datasource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let qty = self.questionModel.question.whatType() == .ChoiceQuestion ? self.questionModel.question.choices.count : 0
        return qty
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! InquirerTableCell
        let answer = self.answers[indexPath.row] as Answer
        cell.questionLabel.text = answer.text.stringByReplacingOccurrencesOfString("[", withString: "").stringByReplacingOccurrencesOfString("]", withString: "")
        cell.letterLabel.text = letters[indexPath.row]
        cell.corretImageView.image = nil

        return cell
        
    }
    
//    MARK: - Table View Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let answer = self.answers[indexPath.row] as Answer
        self.questionModel.givenAnswers.addObject(answer)
        self.questionModel.numGivenAnswers++
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationDidChangeAnswersGiven, object: self.questionModel)
        }
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let answer = self.answers[indexPath.row] as Answer
        self.questionModel.givenAnswers.removeObject(answer)
        if self.questionModel.numGivenAnswers > 0 {
            self.questionModel.numGivenAnswers--
        }
    }
    
    

}

//  MARK: - Text Fields delegate
extension InquirerCollectionCell: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField.hasText() {
            let testString = textField.text!.stringByReplacingOccurrencesOfString(",", withString:".") as String
            if let number = NSNumberFormatter().numberFromString(testString) {
                questionModel.givenAnswers.replaceObjectAtIndex(textField.tag, withObject: number)
            } else {
                textField.text = ""
            }
        }  else {
            questionModel.givenAnswers.replaceObjectAtIndex(textField.tag, withObject: NSNumber(integer: -1))
        }
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationDidChangeAnswersGiven, object: nil)
        }
        
        questionModel.numGivenAnswers = 0
        
        var givenAnswers = questionModel.givenAnswers as [AnyObject]
        
        for obj in givenAnswers {
            if obj is NSNumber {
                questionModel.numGivenAnswers++
            }
        }
        
    }
}