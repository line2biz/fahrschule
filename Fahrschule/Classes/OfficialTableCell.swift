//
//  OfficialTableCell.swift
//  Fahrschule
//

import UIKit

class OfficialTableCell: UITableViewCell {

//    MARK: - Properties
    var answer: Answer? { didSet { configureView() } }
    var questionModel: QuestionModel?
    var questionSheetType: QuestionSheetType = .Exam
    
    
//    MARK: - Initialization
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.textLabel?.numberOfLines = 0
        self.textLabel?.font = UIFont.helveticaNeueRegular(15)
        self.textLabel?.textColor = UIColor.blackColor()
        
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
        let v  = UIView()
        v.backgroundColor = UIColor.clearColor()
        selectedBackgroundView = v
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
//    MARK: - View Life cycle
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        configureView()
    }
    
//    MARK: - Public functins
    func configureView() {
        textLabel?.text = answer?.text.stringByReplacingOccurrencesOfString("[", withString: "").stringByReplacingOccurrencesOfString("]", withString: "")
        imageView?.image = UIImage(named: "checkbox")
        
        if questionSheetType.contains(.History) {
            if selected {
                if answer!.correct.boolValue {
                    imageView?.image = UIImage(named: "checkbox_correct_selected")
                } else {
                    imageView?.image = UIImage(named: "checkbox_incorrect_selected")
                }
                
            } else {
                if answer!.correct.boolValue {
                    imageView?.image = UIImage(named: "checkbox_correct")
                } else {
                    imageView?.image = UIImage(named: "checkbox")
                }
            }
     
            
        } else {
            
            if selected {
                imageView?.image = UIImage(named: "checkbox_selected")
            } else {
                imageView?.image = UIImage(named: "checkbox")
            }
        }
        
    }
    
    

    
    

}
