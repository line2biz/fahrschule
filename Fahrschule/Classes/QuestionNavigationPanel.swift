//
//  CatalogSelectionView.swift
//  Fahrschule
//
//  Created on 22.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

enum QuestionsCatalogType {
    case Main
    case Additive
}

protocol QuestionNavigationPanelDelegate: NSObjectProtocol {
    func navigationPanel(navigationPanel: QuestionNavigationPanel, didSelectItemAtIndexPath indexPath: NSIndexPath)
    func navigationPanel(navigationPanel: QuestionNavigationPanel, isQuestionAnsweredAtIndexPath indexPath: NSIndexPath)->Bool
    func navigationPanel(navigationPanel: QuestionNavigationPanel, isQuestionTaggedAtIndexPath indexPath: NSIndexPath)->Bool

}


class QuestionNavigationPanel: UIView, UICollectionViewDelegate {
//class CatalogSelectionView: UIView, UICollectionViewDelegate {

    let mainGroupCount = 20
    let additiveGroupCount = 10
    
    
    weak var delegate: QuestionNavigationPanelDelegate?
    
    @IBOutlet weak var maingroupButton: UIButton!
    @IBOutlet weak var subgroupButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var questions: [Question]!
//    var modelQuestion
    
    
    var selectedQuestionsCatalog: QuestionsCatalogType = .Main
    
    func selectItemAtIndex(indexPath: NSIndexPath) {
        
        if indexPath.item >= mainGroupCount && selectedQuestionsCatalog == .Main {
            maingroupButton.selected = false
            subgroupButton.selected = true
            selectedQuestionsCatalog = .Additive
            collectionView.reloadData()
            
        }
        else if indexPath.item < mainGroupCount && selectedQuestionsCatalog == .Additive {
            
            maingroupButton.selected = true
            subgroupButton.selected = false
            selectedQuestionsCatalog = .Main
            collectionView.reloadData()
            
            
        }
        
        
        let path = innerIndexPath(forOuterIndexPath: indexPath)
        
        if path.item < collectionView.numberOfItemsInSection(0){
            collectionView.reloadItemsAtIndexPaths([path])
            collectionView.selectItemAtIndexPath(path, animated: true, scrollPosition: .None)
        }
    }
    
    func updateItemAtIndexPath(indexPath: NSIndexPath) {
        let path = innerIndexPath(forOuterIndexPath: indexPath)
        if path.item < collectionView.numberOfItemsInSection(0) {
            if let cell = collectionView.cellForItemAtIndexPath(path) as? QuestionsNavigationCell {
                let selected = cell.selected
                collectionView.reloadItemsAtIndexPaths([path])
                if selected {
                    collectionView.selectItemAtIndexPath(path, animated: false, scrollPosition: .None)
                }
            }
        }
    }
    
    
//    MARK: - Outlet functions
    @IBAction func didTapButton(sender: UIButton) {
        
        if sender.selected {
            return
        }
        
        sender.selected = !sender.selected
        if sender ==  self.maingroupButton {
            self.subgroupButton.selected = !self.maingroupButton.selected
        } else {
            self.maingroupButton.selected = !self.subgroupButton.selected
        }
        
        if self.maingroupButton.selected {
            self.selectedQuestionsCatalog = .Main
            self.collectionView.reloadData()
            
        } else {
            self.selectedQuestionsCatalog = .Additive
            self.collectionView.reloadData()
        }
        
        
        if collectionView.numberOfSections() > 0 {
            if collectionView.numberOfItemsInSection(0) > 0 {
                let indexPath = NSIndexPath(forItem: 0, inSection: 0)
                collectionView.reloadItemsAtIndexPaths([indexPath])
                collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .None)
                delegate?.navigationPanel(self, didSelectItemAtIndexPath: self.outerIndePath(innerIndexPath: indexPath))
            }
        }
        
    }
    


    
//    MARK: - Private functions
    private func outerIndePath(innerIndexPath indexPath: NSIndexPath) -> NSIndexPath {
        if selectedQuestionsCatalog == .Main {
            return indexPath
        } else {
            let settings = Settings.sharedSettings() as! Settings
            switch settings.teachingType {
            case .FirstTimeLicense:
                return NSIndexPath(forItem: mainGroupCount + indexPath.item, inSection: 0)
            default:
                return NSIndexPath(forItem: 10 + indexPath.item, inSection: 0)
            }
            
        }
    }
    
    private func innerIndexPath(forOuterIndexPath indexPath: NSIndexPath) -> NSIndexPath {
        if selectedQuestionsCatalog == .Main {
            return indexPath
        } else {
            let settings = Settings.sharedSettings() as! Settings
            switch settings.teachingType {
            case .FirstTimeLicense:
                return NSIndexPath(forItem: indexPath.item - mainGroupCount, inSection: 0)
            default:
                return NSIndexPath(forItem: indexPath.item - 10, inSection: 0)
            }

        }

    }

}

//    MARK: - Collection View datasource
extension QuestionNavigationPanel: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch selectedQuestionsCatalog {
        case .Main:
            let settings = Settings.sharedSettings() as! Settings
            switch settings.teachingType {
            case .FirstTimeLicense:
                return 20
            default:
                return 10
            }
        default:
            return additiveGroupCount
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! QuestionsNavigationCell
        
        if let answered = delegate?.navigationPanel(self, isQuestionAnsweredAtIndexPath: outerIndePath(innerIndexPath: indexPath)) {
            cell.answered = answered
        }
        
        if let tagged = delegate?.navigationPanel(self, isQuestionTaggedAtIndexPath: outerIndePath(innerIndexPath: indexPath)) {
            cell.tagged = tagged
        }
        
        cell.titleLable.text = "\(indexPath.item + 1)"
        return cell
    }
}

// MARK: - Collection View delegate
extension QuestionNavigationPanel {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        delegate?.navigationPanel(self, didSelectItemAtIndexPath: outerIndePath(innerIndexPath: indexPath))
    }
}
