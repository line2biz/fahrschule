//
//  OverlayView.swift
//  Test
//
//  Created by Шурик on 15.07.15.
//  Copyright (c) 2015 Alexandr Zhovty. All rights reserved.
//

import UIKit

class OverlayView: UIView {

    
    struct OverlayViewOptions {
        var itemSize = CGSizeMake(40, 40)
        var selectedItemSize = CGSizeMake(50, 50)
        var itemWidth: CFloat = 5
    }
    
    var overlayOptions = OverlayViewOptions()
    
    var holeRect = CGRectNull { didSet { self.setNeedsDisplay() }}
    
    var textLabel: UILabel!
    
//    MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        backgroundColor = UIColor.clearColor()
        let recognizer = UITapGestureRecognizer(target: self, action: Selector("handleTap:"))
        addGestureRecognizer(recognizer)
        
        textLabel = UILabel()
        textLabel.textColor = UIColor.whiteColor()
        textLabel.font = UIFont.helveticaNeueRegular(15)
        textLabel.frame = CGRectZero
        textLabel.numberOfLines = 0
        textLabel.backgroundColor = UIColor.clearColor()
        
    }
    
//    MARK: - Drawing
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        let fillColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        
        CGContextSetFillColorWithColor( context, fillColor.CGColor );
        CGContextFillRect( context, rect );
        
        let holeRectIntersection = CGRectIntersection( holeRect, rect );
        
        CGContextSetFillColorWithColor( context, UIColor.clearColor().CGColor );
        CGContextSetBlendMode(context, CGBlendMode.Clear);
        
        CGContextFillEllipseInRect( context, holeRect );
    }
    
//    MARK: - Gesturre
    func handleTap(recognizer: UIGestureRecognizer) {
        let location = recognizer.locationInView(recognizer.view)
        var foundItem: OverlayItem? = nil
        for view in subviews {
            if let itemView = view as? OverlayItem {
                if CGRectContainsPoint(itemView.frame, location) {
                    foundItem = itemView
                    break
                }
            }
        }
        selectOverlayItem(foundItem)
    }
    
//    MARK: - Private funtions
    private func selectOverlayItem(item: OverlayItem?) {
        let selectedItem = selectedOverlayItem()
        
        if item == selectedItem {
            selectedItem?.setSelected(false, animated: true)
            textLabel.removeFromSuperview()
            
        } else {
            selectedItem?.setSelected(false, animated: true)
            if let it = item {
                it.setSelected(true, animated: true)
                
                if let s = it.text {
                    
                    
                    let paragraphStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
                    paragraphStyle.alignment = NSTextAlignment.Center
                    let str = NSAttributedString(string: s, attributes: [ NSFontAttributeName: textLabel.font, NSParagraphStyleAttributeName: paragraphStyle])
                    
                    let minSize = CGSizeMake(250, CGFloat.max)
                    textLabel.attributedText = str
                    let textRect = str.boundingRectWithSize(minSize, options: .UsesLineFragmentOrigin , context: nil)
                    
                    textLabel.frame = textRect
                    textLabel.center = CGPointMake(CGRectGetMidX(item!.frame), CGRectGetMaxY(item!.frame) + CGRectGetMidY(textLabel.bounds))
                    
                    if CGRectGetMaxY(textLabel.frame) > CGRectGetMaxY(bounds) {
                        textLabel.center.y = CGRectGetMinY(item!.frame) - CGRectGetMidY(textLabel.bounds)
                    }
                    
                    // Move label if out of bouds
                    if CGRectGetMinX(textLabel.frame) < 10 {
                        textLabel.frame.origin.x = 10
                    }
                    else if CGRectGetMaxX(textLabel.frame) > CGRectGetMaxX(bounds) - 10 {
                        textLabel.frame.origin.x = CGRectGetMaxX(bounds) - CGRectGetWidth(textLabel.frame) - 10
                    }
                    
                    // Move at the front
                    addSubview(textLabel)
                }
                
            } else {
                textLabel.removeFromSuperview()
            }

        }

    }
    
    private func selectedOverlayItem() -> OverlayItem? {
        for view in subviews {
            if let item = view as? OverlayItem {
                if item.selected {
                    return item
                }
            }
        }
        
        return nil
    }
    

}
