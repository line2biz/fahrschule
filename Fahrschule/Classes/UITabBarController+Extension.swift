//
//  UITabBarController+Extension.swift
//  Fahrschule
//


import Foundation
import GoogleMobileAds


extension UINavigationController {
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.Portrait
        } else {
            return UIInterfaceOrientationMask.Landscape
        }
    }
    
    
    public override func shouldAutorotate() -> Bool {
        return true
    }
}

extension UISplitViewController {
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.Portrait
        } else {
            return UIInterfaceOrientationMask.Landscape
        }
    }
    
    
    public override func shouldAutorotate() -> Bool {
        return true
    }
}

extension UITabBarController {
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.Portrait
        } else {
            return UIInterfaceOrientationMask.Landscape
        }
    }
    
    
    public override func shouldAutorotate() -> Bool {
        return true
    }
}

extension UIViewController {
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: ApplicationGoogleAdsID)
       
        let request: GADRequest = GADRequest()
        request.testDevices = [ kGADSimulatorID, "d11daefd6bf3ab4ac67af8a6d8b8b264" ]
        interstitial?.loadRequest(request)
        
        return interstitial
    }
}