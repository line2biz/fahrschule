//
//  ExamResultViewController.swift
//  Fahrschule
//
//  Created on 29.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

class ExamResultViewController: UIViewController {
//    MARK: - Types
    struct MainStoryboard {
        struct SegueIdentifiers {
            static let showAllQuestions = "showAllQuestions"
            static let showErrorQuestions = "showErrorQuestions"
        }
    }
    
//    MARK: Public properties
    var managedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext!
    var questionModels: [QuestionModel]!
    var timeLeft: Int = 0
    var numQuestionsNotCorrectAnswered: Int = 0
    
    
//    MARK: Private properties
    private var examSheetDictionary: NSDictionary!
    private var maxPoints: Int = 0
    
    
//    MARK: Outlets
    @IBOutlet weak var succeedView: CircularCounterView!
    @IBOutlet weak var failedView: CircularCounterView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var faultsButton: UIButton!
    @IBOutlet weak var allQuestionsButton: UIButton!
    
    // iPhone 5s
    
    @IBOutlet weak var topConstraintImageView: NSLayoutConstraint!
    @IBOutlet weak var verticalConstraintButtons: NSLayoutConstraint!
    
    @IBOutlet weak var topConstraintTitleLabel: NSLayoutConstraint!
    
    @IBOutlet weak var bottomConstraintTitleLabel: NSLayoutConstraint!
    
    @IBOutlet var verticalConstraints: [NSLayoutConstraint]!

    
//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("ExamSheet", ofType: "plist")
        examSheetDictionary = NSDictionary(contentsOfFile: path!)
        
        let key1 = "\(Settings.sharedSettings().licenseClass.rawValue)"
        let key2  = "\(Settings.sharedSettings().teachingType.rawValue)"
        maxPoints = examSheetDictionary.valueForKeyPath("\(key1).\(key2).MaxPoints")!.integerValue
        
        self.navigationItem.hidesBackButton = true
        
        // Statistic
        let examStat = ExamStatistic.insertNewStatistics(managedObjectContext, state: .FinishedExam)
        if let stat = examStat {
            stat.index = 0
            stat.timeLeft = timeLeft
            
        }
        
        
        NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationUpdateBadgeValue, object: nil)
        
        var points: Int = 0
        var mainGroupPoints: Int = 0
        var numFivePointsQuestionFalse: Int = 0
        

        for model in self.questionModels {
            
            if let stat = examStat {
                stat.addStatisticsWithModel(model, inManagedObjectContext: managedObjectContext)
            }
            
            if model.hasAnsweredCorrectly() == false {
                numQuestionsNotCorrectAnswered++
                points += model.question.points.integerValue
                if model.question.points.integerValue == 5 {
                    numFivePointsQuestionFalse++
                }
            }
        }
        
        SNAppDelegate.sharedDelegate().saveContext()
        
        
        if points > maxPoints || (numFivePointsQuestionFalse == 2 && points == 10 && maxPoints == 10) {
            imageView.image = UIImage(named: "durchgefallen")
            titleLabel.text = NSLocalizedString("Nicht bestanden", comment: "")
            succeedView.tintColor = UIColor.roseAshesColor()
            
        } else {
            imageView.image = UIImage(named: "Bestanden")
            titleLabel.text = NSLocalizedString("bestanden", comment: "")
            succeedView.tintColor = UIColor.lemonColor()
        }
            
        if points == 0 {
            faultsButton.enabled = false
        }
        
        succeedView.title = NSLocalizedString("Punkte", comment: "")
        
        
        // Time label
        failedView.tintColor = UIColor.brickColor()
        var time: Float = 60 * 60 - Float(timeLeft);
        if time < 60 {
            failedView.countLabel.text = "\(Int(time))"
            failedView.title = NSLocalizedString("Sekunden", comment: "")
            
        } else {
            time = floorf( time / 60.0)
            failedView.countLabel.text = "\(Int(time))"
            failedView.title = NSLocalizedString("Minuten", comment: "")
            
        }
        
        
        self.succeedView.countLabel.text = "\(points)"
        
        
        
        if points == 0 {
            self.subtitleLabel.text = NSLocalizedString("Fehlerlos! Hervorragend!", comment: "");
        }
        else if (points <= self.maxPoints || (numFivePointsQuestionFalse != 2 && points == 10 && self.maxPoints == 10)) {
            self.subtitleLabel.text = NSLocalizedString("Gratulation, bestanden!", comment: "");
        }
        else if ((points > self.maxPoints && points <= 20) || (numFivePointsQuestionFalse == 2 && points == 10 && self.maxPoints == 10)) {
            self.subtitleLabel.text = NSLocalizedString("Nicht schlecht, dranbleiben!", comment: "");
        }
        else if (points > 20 && points <= 60) {
            self.subtitleLabel.text = NSLocalizedString("Naja - üben, üben, üben!", comment: "");
        }
        else {
            self.subtitleLabel.text = NSLocalizedString("Ohje! Es gibt noch viel zu lernen.", comment: "");
        }
        
        
        // Change interface for ealier models then iPhone 5s
        if CGRectGetHeight(UIScreen.mainScreen().bounds) <= 480 {
            for constraint in verticalConstraints {
                constraint.constant = 5
            }
            
            
        }
        // Change interface for iPhone 5s
        else if CGRectGetHeight(UIScreen.mainScreen().bounds) <= 568 {
            topConstraintImageView.constant = 20
            verticalConstraintButtons.constant = 20
            topConstraintTitleLabel.constant = 10
            bottomConstraintTitleLabel.constant = 10
        }
        
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.toolbarHidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if isIPAD() {
            let splitController = navigationController?.parentViewController as? UISplitViewController
            let masterNavigationController = splitController?.viewControllers.first as? UINavigationController
            if let questionsController = masterNavigationController?.topViewController as? QuestionsTableViewController {
                questionsController.dataSource = questionModels
                questionsController.tableView.reloadData()
            }
        }
        
    }

//    MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Get QuestionSheetController from stack
        let detailController = self.navigationController?.viewControllers.first as! QuestionSheetViewController
        
        let qsvc = segue.destinationViewController as! QuestionSheetViewController
        qsvc.solutionIsShown = true
        if segue.identifier == MainStoryboard.SegueIdentifiers.showAllQuestions {
            qsvc.questionModels = self.questionModels
        } else {
            var arr = [QuestionModel]()
            for model in self.questionModels {
                if model.hasAnsweredCorrectly() == false {
                    arr.append(model)
                }
            }
            qsvc.questionModels = arr
        }
        
        
        qsvc.currentIndexPath = NSIndexPath(forItem: 0, inSection: 0)
        qsvc.currentIndex = 0
        
        
        if isIPAD() {
            let splitController = navigationController?.parentViewController as? UISplitViewController
            let masterNavigationController = splitController?.viewControllers.first as? UINavigationController
            if let questionsController = masterNavigationController?.topViewController as? QuestionsTableViewController {
                questionsController.dataSource = qsvc.questionModels
                questionsController.tableView.reloadData()
            }
        }
       
        
        
        
    }
    
//    MARK: - Outlets functions
    @IBAction func didTapButtonClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func didTapButtonRetry(sender: AnyObject) {
        // Get QuestionSheetController from stack
        let detailController = self.navigationController?.viewControllers.first as! QuestionSheetViewController

        // Delete answered questions
        let models = detailController.questionModels
        for model in models {
            model.givenAnswers = nil
            model.hasSolutionBeenShown = false
        }
        
        
        if isIPAD() { // Update split master controller
            let splitController = navigationController?.parentViewController as? UISplitViewController
            let masterNavigationController = splitController?.viewControllers.first as? UINavigationController
            if let questionsController = masterNavigationController?.topViewController as? QuestionsTableViewController {
                if questionsController.questionSheetType.contains(.History) {
                    questionsController.dataSource = models
                    questionsController.questionSheetType = .Exam
                    questionsController.tableView.reloadData()
                }
            }
        }
        
        // Configure Questions Sheet controller
        detailController.questionModels = models
        detailController.questionSheetType = .Exam
        detailController.currentIndexPath = NSIndexPath(forItem: 0, inSection: 0)
        detailController.currentIndex = 0
        detailController.collectionView?.reloadData()
        detailController.timeLeft = 60 * 60
        detailController.startTimer()
        
        self.navigationController?.popToRootViewControllerAnimated(true)
        
        
    }
    
}
