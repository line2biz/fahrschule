//
//  ContainerController.swift
//  Fahrschule
//

import UIKit

class OfficialQuestionsController: UIViewController {
    
//    MARK: - Types
    struct MainStoryboard {
        struct SegueIdentifiers {
            static let ShowExamResults = "ShowExamResults"
        }
    }
    
    
//    MARK: Public
    var managedObjectContext: NSManagedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext
    var questionModels: [QuestionModel]!
    var questionSheetType: QuestionSheetType = .Learning
    var currentIndexPath: NSIndexPath?
    var timeLeft: Int = 60 * 60
    
//    MARK: Private
    private var examTimer: NSTimer?
    private var pageController: UIPageViewController!
    

//    MARK: Outlets
    @IBOutlet weak var buttonsContainer: UIView!
    @IBOutlet weak var navigationPanel: QuestionNavigationPanel!
    @IBOutlet weak var bottomConstratint: NSLayoutConstraint!
    
    @IBOutlet weak var hidePanelButton: UIButton!
    @IBOutlet weak var showPanelButton: UIButton!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    
//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        title = NSLocalizedString("Prüfung", comment: "")
        
        // Hide navigation panel for iPone interface
        if isIPhone() {
            setNavigationPanelHidden(true, animated: false)
        } else {
            showPanelButton.hidden = true
            hidePanelButton.hidden = true
        }
        
        navigationPanel.delegate = self
        
        // Start Exam timer 
        if questionSheetType.contains(.History) {
            startTimer()
            timerLabel.text = timeLeftString()
        }
        
        // Update interface
        configureView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if let path = currentIndexPath {
            navigationPanel.selectItemAtIndex(path)
            currentIndexPath = nil
        }
    }


//    MARK: - Navigation 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? UIPageViewController {
            pageController = controller
            pageController.view.backgroundColor = UIColor.colorFromHex(0xD2F6DA, alpha: 1)
            pageController.dataSource = self
            pageController.delegate = self
            let controller = questionControllerWithPageIndex(currentIndexPath!.item)!
            pageController.setViewControllers([controller], direction: .Forward, animated: false, completion: nil)
        }
        else if let examResultVC = segue.destinationViewController as? OfficialResultViewController {
            stopTimer()
            
            examResultVC.managedObjectContext = managedObjectContext
            examResultVC.questionModels = questionModels
            examResultVC.timeLeft = timeLeft
        
            SNAppDelegate.sharedDelegate().saveContext()
        
        }
    }


    
    
//    MARK: - Outlet functions
    @IBAction func didTapButtonShowPanel(sender: AnyObject) {
        setNavigationPanelHidden(false, animated: true)
    }
    
    @IBAction func didTapButtonHidePanel(sender: AnyObject) {
        setNavigationPanelHidden(true, animated: true)
    }
    
    
    @IBAction func didTapButtonInterrupt(sender: AnyObject) {
        // Show alert
        let title = NSLocalizedString("Unterbrechen", comment: "")
        let msg = NSLocalizedString("Möchtest du die Prüfung wirklich unterbrechen?", comment: "")
        let yesBtn = NSLocalizedString("Ja",comment: "")
        let noBtn = NSLocalizedString("Nein", comment: "")
        
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        
        // 'NO' action
        alertController.addAction(UIAlertAction(title: noBtn, style: .Cancel, handler: nil))
        
        // 'YES' action
        alertController.addAction(UIAlertAction(title: yesBtn, style: UIAlertActionStyle.Default, handler: { _ in
            // Stop timer
            self.stopTimer()
            
            // Caluclate statistic
            if let examStat = ExamStatistic.insertNewStatistics(self.managedObjectContext, state: .CanceledExam) {
                examStat.index = self.currentPageIndex()
                examStat.timeLeft = self.timeLeft
                
                for model in self.questionModels {
                    examStat.addStatisticsWithModel(model, inManagedObjectContext: self.managedObjectContext)
                }
            }
            
            // Store data into database
            SNAppDelegate.sharedDelegate().saveContext()
            
            // Dismiss controller & update badge number
            self.dismissViewControllerAnimated(true, completion: { _ in
                NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationUpdateBadgeValue, object: nil)
            })
            
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
//    MARK: - Private functions
    let ANIMATION_DURATION = 0.25
    private func setNavigationPanelHidden(hidden: Bool, animated: Bool) {
        
        if hidden {
            bottomConstratint.constant = -CGRectGetHeight(navigationPanel.frame) - CGRectGetHeight(buttonsContainer.frame)
        } else {
            navigationPanel.hidden = false
            buttonsContainer.hidden = false
            bottomConstratint.constant = 0
        }
        
        let animation = {
            self.view.layoutIfNeeded()
        }
        
        let completion = {
            [weak self] (finished: Bool) -> Void in
            if let strongSelf = self {
                strongSelf.navigationPanel.hidden = hidden
                strongSelf.buttonsContainer.hidden = hidden
            }
            
            
        }
        
        if animated {
            UIView.animateWithDuration(ANIMATION_DURATION, animations: animation, completion: completion)
        } else {
            //            animation()
            completion(true)
        }
    }
    
    private func currentPageIndex()->Int {
        if let controller = pageController.viewControllers![0] as? OfficialQuestionController {
            return controller.pageIndex
        } else {
            return 0
        }
    }
    
    private func numberOfAnsweredQuestions()->Int {
        var qty: Int = 0;
        for model in questionModels {
            if model.isAnAnswerGiven() {
                qty++;
            }
        }
        return qty
    }
    
    private func configureView() {
        
        let pageIndex = currentPageIndex()
        let model: QuestionModel = self.questionModels[pageIndex]
        var title = "\(pageIndex + 1)/\(self.questionModels.count) |  \(model.question.points) Pkt."
        if model.question.number.hasSuffix("-M") {
            title += " | M"
        }
        self.navigationItem.title = title
        
        let settings = Settings.sharedSettings() as! Settings
        navigationPanel.subgroupButton.setTitle(settings.licenseClassString, forState: UIControlState.Normal)
       
    }
    
//    MARK: Timer functions
    func startTimer() {
        if let timer = examTimer {
            timer.invalidate()
        }
        
        self.examTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("examTimeLeft:"), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        
        if let timer = examTimer {
            timer.invalidate()
            examTimer = nil
        }
    }
    
    func examTimeLeft(sender: AnyObject) {
        self.timeLeft--
        
        if self.timeLeft % 60 == 0 || self.timeLeft <= 60 {
            if self.timeLeft == 0 {
                handInExamAndShowResult()
            }
            else if self.timeLeft < 60 {
                self.timerLabel.textColor = UIColor.redColor()
            }
            
            
        }
        
        self.timerLabel.text = timeLeftString()
    }
    
    func timeLeftString()->String {
        let left = ceil(Double(self.timeLeft) / 60.0)
        return self.timeLeft <= 60 ? "\(self.timeLeft)" : "\(Int(left)) min";
    }
    
    func handInExamAndShowResult() {
        self.examTimer?.invalidate()
        self.examTimer = nil
        self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.ShowExamResults, sender: self)
    }
}

// MARK: - Navigation Panel delegate
extension OfficialQuestionsController: QuestionNavigationPanelDelegate {
    
    func navigationPanel(navigationPanel: QuestionNavigationPanel, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let controller = questionControllerWithPageIndex(indexPath.item)!
        let currentPage = currentPageIndex()
        if indexPath.item != currentPageIndex() {
            
            let direction: UIPageViewControllerNavigationDirection = indexPath.item > currentPage ? .Forward : .Reverse
            pageController.setViewControllers([controller], direction: direction, animated: true) { _ in
                self.configureView()
            }
        }
        
    }
    
    func navigationPanel(navigationPanel: QuestionNavigationPanel, isQuestionAnsweredAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        if indexPath.item <= questionModels.count {
            let model = questionModels[indexPath.item]
            return model.isAnAnswerGiven()
        }
        return false
    }
    
    func navigationPanel(navigationPanel: QuestionNavigationPanel, isQuestionTaggedAtIndexPath indexPath: NSIndexPath) -> Bool {
        if indexPath.item <= questionModels.count {
            let model = questionModels[indexPath.item]
            return model.isTagged
        }
        return false
    }
}

// MARK: - Page View Controller data source
extension OfficialQuestionsController: UIPageViewControllerDataSource {
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let controller = viewController as! OfficialQuestionController
        if controller.pageIndex == 0 {
            return nil
        } else {
            return questionControllerWithPageIndex(controller.pageIndex - 1)
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let controller = viewController as! OfficialQuestionController
        if controller.pageIndex < questionModels.count - 1 {
            return questionControllerWithPageIndex(controller.pageIndex + 1)
        } else {
            return nil
        }
    }
    
    func questionControllerWithPageIndex(pageIndex: Int)->OfficialQuestionController? {
        let controller =  OfficialQuestionController.controllerWithPageIndex(pageIndex)
        controller.questionModel = questionModels[pageIndex]
        controller.delegate = self
        controller.questionSheetType = questionSheetType
        return controller
    }
    
}


// MARK: - Page View Controller delegate
extension OfficialQuestionsController: UIPageViewControllerDelegate {
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            let lockQueue = dispatch_queue_create("com.Fahrschule.LockQueue", DISPATCH_QUEUE_SERIAL)
            dispatch_sync(lockQueue) {
                let path = NSIndexPath(forItem: self.currentPageIndex(), inSection: 0)
                self.navigationPanel.selectItemAtIndex(path)
                self.configureView()
            }
        }
    }
}


// MARK: - Officail Controller delegate
extension OfficialQuestionsController: OfficialQuestionControllerDelegate {
    
    func officialQuestionController(officialQuestionController: OfficialQuestionController, didChangeAnswerForModel model: QuestionModel) {
        navigationPanel.updateItemAtIndexPath(NSIndexPath(forItem: model.index, inSection: 0))
    }
    
    func numberOfAnsweredQuestionForOfficialQuestionController(officialQuestionController: OfficialQuestionController) -> (questionsCount: Int, answeredCount: Int) {
        
        var numAnsweredQuestions = 0
        
        for model in questionModels! {
            switch model.questionType {
            case .NumberQuestion:
                var givenAnswers = model.givenAnswers as [AnyObject]
                for val in model.givenAnswers {
                    if let number = val as? NSNumber {
                        if number.integerValue != -1 {
                            numAnsweredQuestions++
                        }
                    }
                }
                
            case .ChoiceQuestion:
                if model.givenAnswers.count != 0 {
                    numAnsweredQuestions++
                }
            }
        }
        
        return (questionModels.count, numAnsweredQuestions)
    }
    
    func officialQuestionController(officialQuestionController: OfficialQuestionController, didTapButtonContinueAtIndex pageIndex: Int) {
        
        let lockQueue = dispatch_queue_create("com.Fahrschule.LockQueue", DISPATCH_QUEUE_SERIAL)
        dispatch_sync(lockQueue) {
            let idx = pageIndex + 1
            if idx < self.questionModels.count {
                let controller = self.questionControllerWithPageIndex(pageIndex + 1)
                self.pageController.setViewControllers([controller!], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: {
                    _ in
                    //                self.navigationPanel.selectItemAtIndex(NSIndexPath(forItem: idx, inSection: 0))
                    self.configureView()
                })
                
                self.navigationPanel.selectItemAtIndex(NSIndexPath(forItem: idx, inSection: 0))
            }
        }
        
        
    }
    
    func officialQuestionController(officialQuestionController: OfficialQuestionController, didTapButtonInterruptAtIndex pageIndex: Int) {
        // Number of given answers
        let numAnsweredQuestions = numberOfAnsweredQuestions()
        
        let answersToPass = self.questionModels.count - numAnsweredQuestions
        if answersToPass > 0 {
            
            let title = NSLocalizedString("Abgeben", comment: "")
            let msg = String.localizedStringWithFormat(NSLocalizedString("Du hast %d Fragen noch nicht beantwortet. Möchtest du trotzdem abgeben?", comment: ""), answersToPass)
            let yesBtn = NSLocalizedString("Ja",comment: "")
            let noBtn = NSLocalizedString("Nein", comment: "")
            
            let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
            
            // 'NO' action
            alertController.addAction(UIAlertAction(title: noBtn, style: .Cancel, handler: nil))
            
            // 'YES' action
            alertController.addAction(UIAlertAction(title: yesBtn, style: .Default, handler: { _ in
                self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.ShowExamResults, sender: self)
            }))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.ShowExamResults, sender: self)
        }
        
    }
    
}
