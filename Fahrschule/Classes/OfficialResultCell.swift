//
//  OfficialResultCell.swift
//  Fahrschule
//
//  Created on 24.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

class OfficialResultCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var subtitleLabel: UILabel?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
