//
//  VideoImageView.swift
//  Test
//
//  Created by Шурик on 30.07.15.
//  Copyright (c) 2015 Alexandr Zhovty. All rights reserved.
//

import UIKit
import AVFoundation

import GoogleMobileAds


class VideoImageView: UIView {

    var image: UIImage? { didSet{ showImage() } }
    private var videoURL: NSURL?
    private let imageView = UIImageView()
    
    private var playButton: UIButton? = nil
    var videoPlayer: AVPlayer?
    var videoPlayerLayer: AVPlayerLayer?
    
    weak var bannerView: GADBannerView?
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        clipsToBounds = true
        
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        imageView.backgroundColor = UIColor.clearColor()
        self.addSubview(imageView)
        
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": self, "imageView": imageView]
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[imageView]-0-|", options: NSLayoutFormatOptions.AlignAllCenterY, metrics: nil, views: views)
        self.addConstraints(horizontalConstraints)
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[imageView]-0-|", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: views)
        self.addConstraints(verticalConstraints)
     
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func didTapButtonPlay(sender: UIButton) {
        sender.selected = true
        sender.hidden = true;
        
        videoPlayer!.play()
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerDidReachEndNotificationHandler:", name: AVPlayerItemDidPlayToEndTimeNotification, object: videoPlayer!.currentItem)
        
        
        
    }
    
    func setVideo(videoURL: NSURL?, startImage: UIImage!, endImage: UIImage!) {
        guard let vURL = videoURL else {
            return
        }
        
        imageView.hidden = true;
        bannerView?.removeFromSuperview()
        removeVideoPlayer()

        self.videoURL = vURL

        if let button = playButton {
            button.removeFromSuperview()
            playButton = nil
        }
        
        
        // Add play button
        let button = UIButton()
        button.setImage(UIImage(named: "Icon-Play"), forState: UIControlState.Normal)
        button.setImage(UIImage(named: "Icon-Play"), forState: UIControlState.Selected)
        button.addTarget(self, action: Selector("didTapButtonPlay:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        button.sizeToFit()
        
        self.addSubview(button)
        
        
        button.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        self.addConstraint(horizontalConstraint)
        
        let verticalConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
        self.addConstraint(verticalConstraint)
        
        
        playButton = button
        
        // Add video player
        let playerItem = AVPlayerItem(URL: vURL)
        
        let player = AVPlayer(playerItem: playerItem)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = bounds
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
        playerLayer.needsDisplayOnBoundsChange = true
        
        layer.insertSublayer(playerLayer, atIndex: 0)
        layer.needsDisplayOnBoundsChange = true
        
        videoPlayer = player
        videoPlayerLayer = playerLayer
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let playerLayer = videoPlayerLayer {
            playerLayer.frame = bounds
        }
    }
    
    private func showImage() {
        if let button = playButton {
            playButton?.removeFromSuperview()
            playButton = nil
            imageView.highlightedImage = nil
        }
        
        
        removeVideoPlayer()
        
        imageView.image = image
        imageView.hidden = false
        bannerView?.removeFromSuperview()
        
    }
    
    func showBanner() {
        bannerView?.removeFromSuperview()
        
        bannerView = ({
            let banner = GADBannerView()
            banner.frame.size = kGADAdSizeMediumRectangle.size
            
            banner.adUnitID = ApplicationGoogleAdsID
            banner.rootViewController = SNAppDelegate.sharedDelegate().window.rootViewController
            
            let request: GADRequest = GADRequest()
            request.testDevices = [ kGADSimulatorID, "d11daefd6bf3ab4ac67af8a6d8b8b264" ]
            banner.loadRequest(request)
            banner.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))
            self.addSubview(banner)
            
            banner.translatesAutoresizingMaskIntoConstraints = false
            let horizontalConstraint = NSLayoutConstraint(item: banner, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
            self.addConstraint(horizontalConstraint)
            
            let verticalConstraint = NSLayoutConstraint(item: banner, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
            self.addConstraint(verticalConstraint)
            
            return banner
        })()
    }
    
//    MARK: - Player notifications
    
    func playerDidReachEndNotificationHandler(notification: NSNotification) {
        playButton?.hidden = false
//        imageView.highlighted = true
//        imageView.hidden = false
        removeVideoPlayer()
        
        let playerItem = AVPlayerItem(URL: videoURL!)
        
        let player = AVPlayer(playerItem: playerItem)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = bounds
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
        playerLayer.needsDisplayOnBoundsChange = true
        
        layer.insertSublayer(playerLayer, atIndex: 0)
        layer.needsDisplayOnBoundsChange = true
        
        videoPlayer = player
        videoPlayerLayer = playerLayer
        
    }
    
    func removeVideoPlayer() {
        videoPlayerLayer?.removeFromSuperlayer()
        videoPlayerLayer = nil
        videoPlayer = nil
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    

}
