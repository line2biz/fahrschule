//
//  OfficialQuestionController.swift
//  Fahrschule
//


import UIKit

protocol OfficialQuestionControllerDelegate: NSObjectProtocol {
    func officialQuestionController(officialQuestionController: OfficialQuestionController, didChangeAnswerForModel model: QuestionModel)
    func numberOfAnsweredQuestionForOfficialQuestionController(officialQuestionController: OfficialQuestionController) -> (questionsCount: Int, answeredCount: Int)
    func officialQuestionController(officialQuestionController: OfficialQuestionController, didTapButtonContinueAtIndex pageIndex: Int)
    func officialQuestionController(officialQuestionController: OfficialQuestionController, didTapButtonInterruptAtIndex pageIndex: Int)
}

class OfficialQuestionController: UIViewController {
    
//    MARK: - Types
    
//    MARK: Public properties
    var pageIndex: Int! = 0
    var questionSheetType: QuestionSheetType = .Exam
    var questionModel: QuestionModel! /* { didSet { self.configureView() } } */
    
    weak var delegate: OfficialQuestionControllerDelegate?
    

//    MARK - Privates
    var localObservers: [AnyObject]?
    
    
//    MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    
    // Overlay
    @IBOutlet weak var prefixLabel: UILabel!
    @IBOutlet weak var prefixLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var numberImageView: UIImageView!
    @IBOutlet var numberTextFields: [UITextField]!
    @IBOutlet var numberTextFieldLabels: [UILabel]!
    @IBOutlet weak var numberOverlay: UIView!
    @IBOutlet weak var numberTextFieldPrefixLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    
    
    @IBOutlet weak var imageView: VideoImageView!
    @IBOutlet weak var tableView: UITableView!
    
    // Buttons
    @IBOutlet weak var deliveryButton: UIButton!
    @IBOutlet weak var tagQuestionButton: UIButton!
    @IBOutlet weak var nextQuestionButton: UIButton!
    
    // Info 'Left Time'
    @IBOutlet weak var questionLeftImageView: UIImageView!
    @IBOutlet weak var questionsLeftLabel: UILabel!
    
    var deletedQuestionSeal: UIImageView!
    
    
    

//    MARK: - Initialization
    class func controllerWithPageIndex(pageIndex: Int) -> OfficialQuestionController {
        let storyboard = UIStoryboard(name: "Official", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("OfficialQuestionController") as! OfficialQuestionController
        controller.pageIndex = pageIndex
        return controller
    }
    
    deinit {
        unregisterObservers()
    }
    
//    MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberOverlay.backgroundColor = UIColor.clearColor()
        numberOverlay.hidden = true
        
        deletedQuestionSeal = ({
            let imageView = UIImageView(image: UIImage(named: "deleted_question_seal"))
            imageView.hidden = true
            imageView.contentMode = .Center
            self.view.addSubview(imageView)
            return imageView
        })()
        
        tableView.registerClass( OfficialTableCell.self, forCellReuseIdentifier: "Cell")
        tableView.backgroundColor = UIColor.clearColor()
        
        // Update interface
        configureView()
        
        // Register observers for keyboard
        registerObservers()
        
    }
    
//    MARK: - Observers
    func registerObservers() {
        
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()
        
        if isIPhone() {
            localObservers = [AnyObject]()
            localObservers?.append(center.addObserverForName(UIKeyboardWillShowNotification, object: nil, queue: queue, usingBlock: {
                [weak self] note in
                if let strongSelf = self {
                    strongSelf.keyboardWillShow(note)
                }
                
            }))
            
            localObservers?.append(center.addObserverForName(UIKeyboardWillHideNotification, object: nil, queue: queue, usingBlock: {
                [weak self] note in
                if let strongSelf = self {
                    strongSelf.keyboardWillHide(note)
                }
                
                }))
            
        }
        
    }
    
    func unregisterObservers() {
        let center = NSNotificationCenter.defaultCenter()
        
        if let observers = localObservers {
            for observer in observers {
                center.removeObserver(observer)
            }
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        print("\(__FUNCTION__) \(notification)")
        
        
        let userInfo = notification.userInfo!
        
        // Get information about the animation.
        let animationDuration: NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        // Get keyboard frame
        //let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        // It is better to user 'magic numbers'
        titleLabelTopConstraint.constant = -60
        
        UIView.animateWithDuration(animationDuration, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        // Get information about the animation.
        let animationDuration: NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        // Get keyboard frame
        //let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        // It is better to user 'magic numbers'
        titleLabelTopConstraint.constant = 8
        
        UIView.animateWithDuration(animationDuration, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
//    MARK: - Outlet functions
    @IBAction func didTapButtonDelivery(sender: AnyObject) {
        /// TDOD: SettingsNotificationHandInExamAndShowResult Should be deleted
        delegate?.officialQuestionController(self, didTapButtonInterruptAtIndex: pageIndex)
    }
    
    @IBAction func didTapButtonMark(sender: UIButton) {
        sender.selected = !sender.selected
        questionModel.isTagged = sender.selected
        delegate?.officialQuestionController(self, didChangeAnswerForModel: questionModel)
        
        /// TDOD: SettingsNotificationDidTagQuestionOfficial Should be deleted
        
    }
    
    @IBAction func didTapButtonContinue(sender: AnyObject) {
        
        delegate?.officialQuestionController(self, didTapButtonContinueAtIndex: pageIndex)
        
        /// TDOD: SettingsNotificationNextQuestion Should be deleted
        
//        NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationNextQuestion, object: questionModel)
        
        
        
    }
    
//    MARK: - Private functions
    private func configureView() {
        if questionModel.question.unused.boolValue {
            deletedQuestionSeal.hidden = false
            deletedQuestionSeal.frame = view.bounds
        }
        
        var givenAnswers = questionModel.givenAnswers as [AnyObject]
        questionModel.numGivenAnswers = UInt(givenAnswers.count)
        
        titleLabel.text = questionModel.question.text

        let imageName = self.questionModel.question.image
        if imageName.characters.count > 0 {
            
            if let idx = imageName.rangeOfString(".mp4") {
                let url = NSBundle.mainBundle().URLForResource("/fahrschule-videos/\(imageName)", withExtension: "")
                imageView.setVideo(url, startImage: nil, endImage: nil)
            } else {
                imageView.image = UIImage(named: imageName)
            }
            
        } else {
            imageView.image = nil
            imageView.showBanner()
        }
        
        if questionModel.question.prefix != "" {
//            prefixLabel.hidden = false
            prefixLabel.text = questionModel.question.prefix
        } else {
//            prefixLabel.hidden = true
            prefixLabelHeightConstraint.constant = 0
            view.layoutIfNeeded()
        }
        
        tagQuestionButton.selected = questionModel.isTagged;
        tableView.reloadData()
        
        // This question is a number question
        
        if questionModel.question.whatType() == .NumberQuestion {

            if questionModel.question.number == "11.11.1" { // The one single special case where a question needs two numbers.
                numberTextFields[1].hidden = false
                numberTextFieldLabels[1].hidden = false
                
            }
            
            
            tableView.hidden = true
            let set = questionModel.question.choices as NSSet
            let answer = set.allObjects[0] as! Answer
            let numberTextField = self.numberTextFields[0]
            numberLabel.text = answer.text
            numberTextField.delegate = self
            
            numberOverlay.hidden = false
            
            
            if questionSheetType.contains(.History) {
                let set = questionModel.question.choices as NSSet
                let answer = set.allObjects[0] as! Answer
                if questionModel.question.answerState(givenAnswers) == .FaultyAnswered {
                    numberImageView.image = UIImage(named: "falsch")
                }
                else {
                    numberImageView.image = UIImage(named: "richtig")
                }
                
                
                let numberTextField = numberTextFields[0];
                self.numberLabel.hidden = false
                numberTextField.enabled = false
                
                if questionModel.givenAnswers.count > 0 &&  givenAnswers[0] is NSNumber {
                    let value = givenAnswers[0] as! NSNumber
                    numberTextField.text = value.integerValue == -1 ? "" : value.stringValue
                }
                
                if numberTextFields[1].hidden == false {
                    let correctNumbers = answer.correctNumber.stringValue.componentsSeparatedByString(".")
                    numberLabel.text = answer.text.stringByReplacingOccurrencesOfString("X", withString: correctNumbers[0]).stringByReplacingOccurrencesOfString("Y", withString: correctNumbers[1])
                    
                    if let value = givenAnswers[1] as? NSNumber {
                        let secondNumberTextField = numberTextFields[1]
                        secondNumberTextField.text = value.intValue == -1 ? "" : value.stringValue
                        secondNumberTextField.enabled = false
                    }
                }
                else {
                    numberLabel.text = answer.text.stringByReplacingOccurrencesOfString("X", withString:answer.correctNumber.stringValue)
                }
                
                
            } else {
                if givenAnswers.count > 0 && givenAnswers[0] is NSNumber {
                    let value = givenAnswers[0] as! NSNumber
                    numberTextField.text = value.intValue == -1 ? "" : value.stringValue
                }
                
                if givenAnswers.count > 1 && givenAnswers[1] is NSNumber {
                    let secondNumberTextField = numberTextFields[1]
                    let value = givenAnswers[1] as! NSNumber
                    secondNumberTextField.text = value.intValue == -1 ? "" : value.stringValue
                    
                }
                
                
                
                if answer.text.hasPrefix("X ") {
                    numberLabel.hidden = !questionModel.hasSolutionBeenShown
                    numberTextFieldPrefixLabel.text = answer.text.stringByReplacingOccurrencesOfString("X ", withString:"")
                }
                else {
                    numberTextFieldPrefixLabel.hidden = true;
                }
                
                // Unofficial should 'X ='
                numberTextFieldLabels[0].text = "Antwort:"
            }
            
        } else {
            tableView.hidden = false
            numberOverlay.hidden = true
            
            
            
            let answers: [Answer] = (questionModel.question.rearrangedChoices as NSSet).allObjects as! [Answer]
            for var i = 0; i < answers.count; i++ {
                let answer = answers[i]
                let indexPath = NSIndexPath(forRow: i, inSection: 0)
                if (givenAnswers as! [Answer]).contains(answer) {
                    tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
                }
            }
            
            
            
        }
        
        updateQuestionsLeftLabel()
        
        
        // Disable editing and selectrion during history displaying
        if questionSheetType.contains(.History) {
            tableView.userInteractionEnabled = false
            numberOverlay.userInteractionEnabled = false
            deliveryButton.hidden = true
            
        }
    }
    
    func updateQuestionsLeftLabel() {
        
        let params = delegate?.numberOfAnsweredQuestionForOfficialQuestionController(self)
        
        let questionsLeft = params!.questionsCount - params!.answeredCount
        
        
        questionLeftImageView.hidden = questionsLeft == 0;
        questionsLeftLabel.hidden = questionsLeft == 0;
        questionsLeftLabel.text = "noch \(questionsLeft) Aufgaben"
        
        
        tagQuestionButton.selected = questionModel.isTagged
        
        // Disable next question button if official exam layout is enabled and the last question is selected
        if questionModel.index != params!.questionsCount - 1 {
            nextQuestionButton.userInteractionEnabled = true
            nextQuestionButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            
        }  else {
            nextQuestionButton.userInteractionEnabled = false
            nextQuestionButton.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            
        }
        
        
    }

}

//    MARK: - Text Field delegate
extension OfficialQuestionController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField.hasText() {
            let testString = textField.text!.stringByReplacingOccurrencesOfString(",", withString:".") as String
            if let number = NSNumberFormatter().numberFromString(testString) {
                questionModel.givenAnswers.replaceObjectAtIndex(textField.tag, withObject: number)
            } else {
                textField.text = ""
            }
            
        }
        else {
            questionModel.givenAnswers.replaceObjectAtIndex(textField.tag, withObject: NSNumber(integer: -1))
            
        }
        
        // Official
        let center = NSNotificationCenter.defaultCenter()
        center.postNotificationName("updateQuestionsLeftLabel", object: nil)
        updateQuestionsLeftLabel()
        
        
//        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
//            center.postNotificationName(SettingsNotificationDidChangeAnswersGiven, object: nil)
//        }
        
        delegate?.officialQuestionController(self, didChangeAnswerForModel: questionModel)
        
        questionModel.numGivenAnswers = 0
        
        var givenAnswers = questionModel.givenAnswers as [AnyObject]
        
        for obj in givenAnswers {
            if obj is NSNumber {
                questionModel.numGivenAnswers++
            }
        }
        
    }
}

//    MARK: - Table View datat source
extension OfficialQuestionController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionModel.question.whatType() == .ChoiceQuestion ? questionModel.question.choices.count : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! OfficialTableCell
        let answer = (questionModel.question.rearrangedChoices as NSSet).allObjects[indexPath.row] as! Answer
        cell.questionSheetType = questionSheetType
        cell.answer = answer
        return cell;
        
    }
}

//    MARK: - Table View delegate
extension OfficialQuestionController: UITableViewDelegate {
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let answer = (questionModel.question.rearrangedChoices as NSSet).allObjects[indexPath.row] as! Answer
        questionModel.givenAnswers.removeObject(answer)
        questionModel.numGivenAnswers--
        updateQuestionsLeftLabel()
        delegate?.officialQuestionController(self, didChangeAnswerForModel: questionModel)

    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let answer = (questionModel.question.rearrangedChoices as NSSet).allObjects[indexPath.row] as! Answer
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! OfficialTableCell
        questionModel.givenAnswers.addObject(answer)
        questionModel.numGivenAnswers++
        updateQuestionsLeftLabel()
        delegate?.officialQuestionController(self, didChangeAnswerForModel: questionModel)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGRectGetHeight(tableView.bounds) / 3
    }
}
