//
//  HelpController.swift
//  Fahrschule
//
//  Created on 14.07.15.
//  Copyright (c) 2015:. All rights reserved.
//

import UIKit

class HelpController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

//    MARK: - Types
    struct MainStoryboard {
        struct CellIdentifiers {
            static let Cell = "Cell"
        }
    }
    
//    MARK: Properties
    var items: [AnyObject]!
    
//    MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
//    MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Flow Layout
        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .Horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        collectionView?.pagingEnabled = true
        collectionView?.collectionViewLayout = flowLayout
        
        
        let bundle = NSBundle.mainBundle()
        let path = bundle.pathForResource("Anleitung", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path)
        items = dict?.valueForKey("ipad") as? [AnyObject]
        

    }
    
//    MARK: - Outlet functions
    @IBAction func didTapButtonClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
//    MARK: - Collection View delegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MainStoryboard.CellIdentifiers.Cell, forIndexPath: indexPath) as! HelpCollectionCell
        let imageName = "helpscreen0\(indexPath.item + 1)"
        cell.imageView.image = UIImage(named: imageName)
        cell.overlays = items[indexPath.row] as! [[String: AnyObject]]
        return cell
    }
    
    //    MARK: - Collection Flow layout delegate
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), CGRectGetHeight(collectionView.bounds) - collectionView.contentInset.top - collectionView.contentInset.bottom)
    }
}
