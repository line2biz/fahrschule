//
//  SplitViewController.swift
//  Fahrschule
//
//  Created on 06.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {
//   Mark: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = SNAppDelegate.sharedDelegate()
    }
}
