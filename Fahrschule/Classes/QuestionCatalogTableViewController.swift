//
//  QuestionCatalogTableViewController.swift
//  Fahrschule
//
//  Created 19.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit
import GoogleMobileAds

class QuestionCatalogTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UISearchControllerDelegate {
    
//    MARK: - Types
    struct MainStoryboard {
        struct ViewControllerTitle {
            static let questionnaire = NSLocalizedString("Fragenkatalog", comment: "")
            static let marked = NSLocalizedString("Markierte", comment: "")
        }
        
        struct ViewControllerIdentifier {
            static let SubGroup = "SubGroupTableViewController"
            static let QuestionsList = "QuestionsTableViewController"
            static let Questions = "QuestionSheetViewController"
        }
        
        struct TableViewCellIdentifiers {
            static let progressCellIdentifier = "Cell"
        }
        
        struct TableViewDatasource {
            static let numberOfSections: Int = 2
        }
        
        struct SegueIdentifiers {
            static let showListSubSubgroup = "SubGroupTableViewController"
            static let showQuestionnaire = "QuestionSheetViewController"
        }
    }
    
//   MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext
    var lastUpdate = NSDate()
    var mainGroupArray: [MainGroup]!
    var dataSource: [MainGroup]!
    var numberOfThemes = [0, 0]
    var progressDict = [Int: ProgressItem]()
    var localObservers = [AnyObject]()
    var questionsController: UIViewController?
    var detailNavigationController: UINavigationController?
    
    
    

//    Search
    var searchController: UISearchController!
    var resultsController: QuestionsTableViewController!
    
//    MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    
    // Google banner
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
//    MARK: - Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.registerObservers();
    }
    
    deinit {
        self.unregisterObservers()
    }
    
    
//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Fragenkatalog", comment: "")
        
        // TableView settings       
        tableView.estimatedRowHeight = 44.0
        
        // Grouprs
        var mainGroupsArray = [MainGroup]()
        let mainGroups = MainGroup.mainGroupsInManagedObjectContext(self.managedObjectContext) as! [MainGroup]
        for mainGroup in mainGroups {
            let numOfQuestions = Question.countQuestionsInRelationsTo(mainGroup, inManagedObjectContext: managedObjectContext)
            if numOfQuestions > 0 {
                mainGroupsArray.append(mainGroup)
                if mainGroup.baseMaterial.boolValue == true {
                    numberOfThemes[0]++
                } else {
                    numberOfThemes[1]++
                }
            }
        }
        
        dataSource = mainGroupsArray
        mainGroupArray = self.dataSource
        
        
        // Searh Results Controller
        (searchController, resultsController) = ({
            let resultsController = self.storyboard!.instantiateViewControllerWithIdentifier("QuestionsTableViewController") as! QuestionsTableViewController
            resultsController.tableView.delegate = self
            
            let navController = UINavigationController(rootViewController: resultsController)
            let controller = UISearchController(searchResultsController: UINavigationController(rootViewController: resultsController))
            controller.searchResultsUpdater = resultsController
            controller.searchBar.sizeToFit()
            self.tableView.tableHeaderView = controller.searchBar
            self.definesPresentationContext = true;  // know where you want UISearchController to be displayed
            return (controller, resultsController)
            
        })()
        
        // Configure for iPad
        if isIPAD() {
            navigationController?.delegate = self
            detailNavigationController?.delegate = self
        }
        
     
        // Google banner
        bannerView.adUnitID = ApplicationGoogleAdsID
        bannerView.rootViewController = self;
        let request: GADRequest = GADRequest()
        request.testDevices = [ kGADSimulatorID, "d11daefd6bf3ab4ac67af8a6d8b8b264" ]
        bannerView.loadRequest(request)
//        tableView.contentInset.bottom = CGRectGetHeight(bannerView.frame)
        

        self.tableView.reloadData()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        
        // Update stastic
        var statisticsLastUpdated = NSDate.distantPast() 
        if  let stat = LearningStatistic.latestStatistic(self.managedObjectContext) {
            statisticsLastUpdated = stat.date
        }
        
        if statisticsLastUpdated.earlierDate(self.lastUpdate).compare(self.lastUpdate) == .OrderedSame {
            self.progressDict.removeAll(keepCapacity: false)
            self.lastUpdate = NSDate()
            self.tableView.reloadData()
        }
        
        // Segmented control
        let taggedQuestions: [Question] = (Question.taggedQuestionsInManagedObjectContext(self.managedObjectContext) as! [Question])
        self.segmentedControl.setEnabled(taggedQuestions.count > 0, forSegmentAtIndex: 1)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.mainGroupArray.count != self.progressDict.count {
            for (i, element) in self.mainGroupArray.enumerate() {
                if self.progressDict[i] == nil {
                    self.progressItemForMainGroupAtIndex(i)
                }
            }
        }
        
    }
    
    //    MARK: - Observers
    private func registerObservers() {
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()
        
        
        self.localObservers.append(center.addObserverForName("tagQuestion", object: nil, queue: queue) {
            [weak self] _ in
            if let weakSelf = self {
                let questions = Question.taggedQuestionsInManagedObjectContext(weakSelf.managedObjectContext)
                if questions.count > 0 {
                    weakSelf.segmentedControl.setEnabled(true, forSegmentAtIndex: 1)
                } else {
                    weakSelf.segmentedControl.setEnabled(false, forSegmentAtIndex: 1)
                }
            }
            })
        
    }
    
    private func unregisterObservers() {
        let center = NSNotificationCenter.defaultCenter()
        for observer in self.localObservers {
            center.removeObserver(observer)
        }
    }
    

//    MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? SubGroupTableViewController {
            let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
            var offset = indexPath.section == 1 ? numberOfThemes[0] : 0
            offset += indexPath.row
            let mainGroup = self.dataSource[offset]
            vc.managedObjectContext = self.managedObjectContext
            vc.mainGroup = mainGroup
            vc.title = mainGroup.name
        }
        else if segue.identifier == MainStoryboard.SegueIdentifiers.showQuestionnaire {
            if let navController = segue.destinationViewController as? UINavigationController {
                if let qsvc = navController.topViewController as? QuestionSheetViewController {
                    qsvc.managedObjectContext = self.managedObjectContext
                    if let arr = sender as? [QuestionModel] {
                        qsvc.questionModels = arr
                    }
                }
            }
        }
    }
    
//    MARK: - Outlet functions
    
    @IBAction func segmentedControlValueChanged(sender: UISegmentedControl) {
        // Change interface layout
        let side1Visible = self.segmentedControl.selectedSegmentIndex == 1
        
        if side1Visible {
            
            self.title = MainStoryboard.ViewControllerTitle.questionnaire
            
            // Show QuestionsTableViewController with Tagged Questions
            let qtvc: QuestionsTableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("QuestionsTableViewController") as! QuestionsTableViewController
            qtvc.view.hidden = true
            qtvc.managedObjectContext = self.managedObjectContext
            qtvc.tableView.contentInset = self.tableView.contentInset
            qtvc.favorites = true
            let taggedQuestions: [Question] = (Question.taggedQuestionsInManagedObjectContext(self.managedObjectContext) as! [Question])
            qtvc.dataSource = QuestionModel.modelsForQuestions(taggedQuestions) as! [QuestionModel]
            
            // Adding to controller to hierarchy
            self.addChildViewController(qtvc)
            qtvc.view.frame = self.containerView.bounds
            self.containerView.addSubview(qtvc.view)
            qtvc.didMoveToParentViewController(self)
            
            self.questionsController = qtvc
            
            // Present with animation
            let options: UIViewAnimationOptions = [UIViewAnimationOptions.BeginFromCurrentState, UIViewAnimationOptions.TransitionFlipFromRight]
            UIView.transitionWithView(self.containerView, duration: 1, options: options, animations: { () -> Void in
                self.tableView.hidden = true
                qtvc.view.hidden = false
                }, completion: { [weak self] _ in
                    
                    if let strongSelf = self {
                        qtvc.masterNavigationController = strongSelf.navigationController
                        qtvc.detailNavigationController = strongSelf.detailNavigationController
                    }
            
            })
            
            
        } else {
            self.title = MainStoryboard.ViewControllerTitle.marked
            
            // Remove controller from hierarchy
            let options: UIViewAnimationOptions = [UIViewAnimationOptions.BeginFromCurrentState, UIViewAnimationOptions.TransitionFlipFromLeft]
            UIView.transitionWithView(self.containerView, duration: 1, options: options, animations: { () -> Void in
                self.questionsController?.view.hidden = true
                self.tableView.hidden = false
            }, completion: { (finished) -> Void in
                
                self.questionsController?.willMoveToParentViewController(nil)
                self.questionsController?.view.removeFromSuperview()
                self.questionsController?.removeFromParentViewController()
                self.questionsController = nil
                
            })
        }
    }

    
    @IBAction func didTapButtonQuery(sender: UIBarButtonItem) {
        
        let title = NSLocalizedString("Abfragen", comment: "")
        let msg = NSLocalizedString("Was soll abgefragt werden?", comment: "")
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Abbrechen", comment: ""), style: .Cancel, handler: nil))
        
        
        // All questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle Fragen", comment: ""), style: .Default, handler: { alertAction in
            var questions: [AnyObject]
            if self.isCatalogSelected() {
                questions = Question.questionsInRelationsTo(nil, inManagedObjectContext: self.managedObjectContext)
            } else {
                questions = Question.taggedQuestionsInManagedObjectContext(self.managedObjectContext)
            }
            self.openQuestionSheetController(alertAction.title!, questions: questions as! [Question])
            
        }))
        
        // Incorrect answered questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle falsch Beantworteten", comment: ""), style: .Default, handler: { alertAction in
            var questions: [AnyObject]
            if self.isCatalogSelected() {
                questions = Question.questionsInRelationsTo(nil, state: .FaultyAnswered, inManagedObjectContext: self.managedObjectContext)
            } else {
                questions = Question.taggedQuestionsInManagedObjectContext(self.managedObjectContext, state: .FaultyAnswered)
            }
            
            self.openQuestionSheetController(alertAction.title!, questions: questions as! [Question])
            
        }))
        
        // Unanswered questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle Unbeantworteten", comment: ""), style: .Default, handler: { alertAction in
            var questions: [AnyObject]
            if self.isCatalogSelected() {
                questions = Question.questionsInRelationsTo(nil, state: .StateLess, inManagedObjectContext: self.managedObjectContext)
            } else {
                questions = Question.taggedQuestionsInManagedObjectContext(self.managedObjectContext, state: .StateLess)
            }
            
            self.openQuestionSheetController(alertAction.title!, questions: questions as! [Question])
            
        }))
        
        // Incorrect and Unanswered questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Falsch & Unbeantwortete", comment: ""), style: .Default, handler: { alertAction in
            var questions: [AnyObject]
            if self.isCatalogSelected() {
                questions = Question.questionsInRelationsTo(nil, state: .FaultyAnswered, inManagedObjectContext: self.managedObjectContext)
                questions += Question.questionsInRelationsTo(nil, state: .StateLess, inManagedObjectContext: self.managedObjectContext)
            } else {
                questions = Question.taggedQuestionsInManagedObjectContext(self.managedObjectContext, state: .FaultyAnswered)
                questions += Question.taggedQuestionsInManagedObjectContext(self.managedObjectContext, state: .StateLess)
            }
            
            self.openQuestionSheetController(alertAction.title!, questions: questions as! [Question])
            
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    
    
//    MARK: - Table View datasource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return MainStoryboard.TableViewDatasource.numberOfSections
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfThemes[section]
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Grundstoff", comment: "");
        case 1:
            return NSLocalizedString("Zusatzstoff", comment: "");
        default:
            return nil
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(MainStoryboard.TableViewCellIdentifiers.progressCellIdentifier, forIndexPath: indexPath) 
        let idx = indexPath.section * numberOfThemes[0] + indexPath.row
        let mainGroup = self.dataSource[idx]
        cell.textLabel!.text = mainGroup.name
        cell.imageView?.image = mainGroup.mainGroupImage()
        
        let progressView = UIView(frame: CGRectMake(0, 0, 70, 10))
        progressView.backgroundColor = UIColor.colorFromHex(0x616161, alpha: 1)
        cell.accessoryView = progressView
        
        var numOfQuestions: UInt = 0
        var correctAnswers: UInt = 0
        var faultyAnswers: UInt = 0
        
        var progressItem = progressDict[idx]
        if progressItem == nil {
            progressItem = progressItemForMainGroupAtIndex(idx)
        }
        
        if progressItem!.correctAnswers > 0 || progressItem!.faultyAnswers > 0 {
            if progressItem!.numOfQuestions > 0 {
                let total = CGFloat(progressItem!.numOfQuestions)
                let correct = CGFloat(progressItem!.correctAnswers)
                let faulty = CGFloat(progressItem!.faultyAnswers)
                
                let succedWidth = correct / total * CGRectGetWidth(progressView.bounds)
                let failedWidth = faulty / total * CGRectGetWidth(progressView.bounds)
                
                let correctView = UIView()
                correctView.backgroundColor = UIColor.lemonColor()
                correctView.frame = progressView.bounds
                correctView.frame.size.width = succedWidth
                
                progressView.addSubview(correctView);
                
                let faultyView = UIView()
                faultyView.backgroundColor = UIColor.roseAshesColor()
                faultyView.frame = progressView.bounds
                faultyView.frame.size.width = failedWidth
                faultyView.frame.origin.x = CGRectGetWidth(correctView.frame)
                
                progressView.addSubview(faultyView);
                
            }
        }
        
        return cell
    }
    
//    MARK: - Table View delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == resultsController.tableView {
            // Search results
            
            let questionarieController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.Questions) as! QuestionSheetViewController
            questionarieController.managedObjectContext = managedObjectContext
            questionarieController.questionModels = resultsController.dataSource
            questionarieController.currentIndexPath = indexPath
            
            if isIPAD() {
                // Update master controller
                let controller = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.QuestionsList) as! QuestionsTableViewController
                controller.title = NSLocalizedString("Suchergebnisse", comment: "")
                controller.managedObjectContext = managedObjectContext
                controller.dataSource = resultsController.dataSource
                controller.questionSheetType = QuestionSheetType.Learning
                controller.currentIndexPath = indexPath

                navigationController?.pushViewController(controller, animated: true)
                
                // Update detail controller
                detailNavigationController?.pushViewController(questionarieController, animated: true)
                
            } else {
                
                let navController = UINavigationController(rootViewController: questionarieController)
                presentViewController(navController, animated: true, completion: nil)
            }
            
            
            
        } else {
            // Subgroup controller
            let subgroupController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.SubGroup) as! SubGroupTableViewController
            var offset = indexPath.section == 1 ? numberOfThemes[0] : 0
            offset += indexPath.row
            let mainGroup = self.dataSource[offset]
            subgroupController.managedObjectContext = self.managedObjectContext
            subgroupController.mainGroup = mainGroup
            subgroupController.title = mainGroup.name
            
            switch UIDevice.currentDevice().userInterfaceIdiom {
            case .Pad: // Show in detail
                if let topController = detailNavigationController?.topViewController as? SubGroupTableViewController {
                    topController.mainGroup = mainGroup
                    topController.title = mainGroup.name
                    topController.tableView.reloadData()
                    
                } else {
                    detailNavigationController?.delegate = self
                    subgroupController.navigationItem.hidesBackButton = true
                    detailNavigationController?.pushViewController(subgroupController, animated: true)
                    
                }
            case .Phone:
                navigationController?.pushViewController(subgroupController, animated: true)
            default: return
            }
            
        }
        
        
        
        
    }
    
//    MARK: - Private Functions
    private func progressItemForMainGroupAtIndex(index: Int)->ProgressItem {
        let mainGroup = self.mainGroupArray[index]
        let item = ProgressItem()
        
        item.numOfQuestions = UInt(Question.countQuestionsInRelationsTo(mainGroup, inManagedObjectContext: self.managedObjectContext))
        if item.numOfQuestions > 0 {
            item.correctAnswers = LearningStatistic.countStatisticsInRelationsTo(mainGroup, inManagedObjectContext: self.managedObjectContext, showState: .CorrectAnswered)
            item.faultyAnswers = LearningStatistic.countStatisticsInRelationsTo(mainGroup, inManagedObjectContext: self.managedObjectContext, showState: .FaultyAnswered)
        }
        
        self.progressDict[index] = item
        return item
    }
    
    private func isCatalogSelected()->Bool {
        return self.segmentedControl.selectedSegmentIndex == 0
    }
    
    
//    MARK: - Navigation Controller delegate
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        // Show the first controller in the detail stack
        if navigationController == self.navigationController && viewController == self {
            detailNavigationController?.popToRootViewControllerAnimated(true)
        }
        else if navigationController == detailNavigationController && viewController is SubGroupTableViewController && self.navigationController?.topViewController != self {
            self.navigationController!.popToRootViewControllerAnimated(true)
        }
    }
    
    func navigationController(navigationController: UINavigationController, didShowViewController viewController: UIViewController, animated: Bool) {
        // Show the first controller in the detail stack
        if navigationController == detailNavigationController {
            if navigationController.viewControllers.count == 1 {
                if let indexPath = tableView.indexPathForSelectedRow {
                    tableView.deselectRowAtIndexPath(indexPath, animated: true)
                }
            }
        }
    }
    
    
//    MARK: - Search controller delegate
    func didDismissSearchController(searchController: UISearchController) {
        if isIPAD() {
            if let splitController = navigationController?.parentViewController as? UISplitViewController {
                let detailNavigatinController = splitController.viewControllers.last as? UINavigationController
                detailNavigationController?.popToRootViewControllerAnimated(true)
            }
        }
    }
    
    //    MARK: - Private functions
    private func openQuestionSheetController(title: String, questions: [Question]) {
        
        if let models = QuestionModel.modelsForQuestions(questions) as? [QuestionModel] {
            if models.count > 0 {
                
                // Detail controller
                let questionarieController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.Questions) as! QuestionSheetViewController
                questionarieController.managedObjectContext = managedObjectContext
                questionarieController.questionModels = models
                questionarieController.currentIndexPath = NSIndexPath(forRow: 0, inSection: 0)
                
                if isIPAD() {
                    
                    // Master controller
                    let questionsController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.QuestionsList) as! QuestionsTableViewController
                    questionsController.title = title
                    questionsController.dataSource = models
                    questionsController.managedObjectContext = managedObjectContext
                    questionsController.questionSheetType = QuestionSheetType.Learning
                    
                    
                    navigationController!.pushViewController(questionsController, animated: true)
                    detailNavigationController?.pushViewController(questionarieController, animated: true)
                    
                } else {
                    let navController = UINavigationController(rootViewController: questionarieController)
                    presentViewController(navController, animated: true, completion: nil)
                }
                
                
                
            }
        }
    }
    
    
}
