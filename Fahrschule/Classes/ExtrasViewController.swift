//
//  ExtrasViewController.swift
//  Fahrschule
//
//  Created on 01.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ExtrasViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
//    MARK: - Types
    struct MainStoryboard {
        struct ControllerIdentifiers {
            static let TrafficSigns = "TrafficSignsViewController"
            static let BrakingDistance = "BrakingDistanceViewController"
            static let Formulas = "FormulasViewController"
            static let Regulations = "RegulationsController"
            static let Help = "HelpController"
        }
        
        struct CellIdentifiers {
            static let mainCell = "Cell"
        }
        
        struct IndexPath {
//            static let Help = NSIndexPath(forItem: 0, inSection: 10)
            static let Rate = NSIndexPath(forItem: 4, inSection: 0)
        }
    }
    
//    MARK:  Public properties
    var dataSource: [[String : String]]!
    
//    MARK: - Private properties
    var bannerView: GADBannerView!
    
//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clearsSelectionOnViewWillAppear = true
        
        let bundle = NSBundle.mainBundle()
        #if FAHRSCHULE_LITE
            let path = bundle.pathForResource("ExtrasLite", ofType: "plist")
            #else
            let path = bundle.pathForResource("Extras", ofType: "plist")
        #endif

        self.dataSource = NSArray(contentsOfFile: path!) as! [[String : String]]
        
        // Google banner
        bannerView = ({
            let banner = GADBannerView()
            banner.frame.size = kGADAdSizeBanner.size
            
            banner.adUnitID = ApplicationGoogleAdsID
            banner.rootViewController = self
            
            let request: GADRequest = GADRequest()
            request.testDevices = [ kGADSimulatorID, "d11daefd6bf3ab4ac67af8a6d8b8b264" ]
            banner.loadRequest(request)
            
            self.view.addSubview(banner)
            
            return banner
        })()
        collectionView!.contentInset.bottom = CGRectGetHeight(bannerView.frame)
        
    }


//    MARK: - Navigation
    @IBAction func exitToExtrasViewController(sender: UIStoryboardSegue) {}
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        
        let indexPath = self.collectionView?.indexPathsForSelectedItems()!.first as NSIndexPath!
        
        switch indexPath {
        case MainStoryboard.IndexPath.Rate:
            let sURL = Settings.sharedSettings().iTunesLink
            UIApplication.sharedApplication().openURL(NSURL(string: sURL)!)
            self.collectionView?.deselectItemAtIndexPath(indexPath, animated: true)
            return false
            
        default:
            return true
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let splitController = segue.destinationViewController as? UISplitViewController {
            let indexPath = self.collectionView?.indexPathsForSelectedItems()!.first as NSIndexPath!
            
            let masterNavController = splitController.viewControllers.first as? UINavigationController
            let masterController = masterNavController?.topViewController as! ExtraTableController
            masterController.dataSource = self.dataSource
            masterController.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
            
            var detailController: UIViewController
            switch indexPath.row {
            case 0:
                detailController = self.storyboard!.instantiateViewControllerWithIdentifier(MainStoryboard.ControllerIdentifiers.Formulas) 
            case 1:
                detailController = self.storyboard!.instantiateViewControllerWithIdentifier(MainStoryboard.ControllerIdentifiers.BrakingDistance) 
            case 2:
                detailController = self.storyboard!.instantiateViewControllerWithIdentifier(MainStoryboard.ControllerIdentifiers.TrafficSigns) 
            default:
                detailController = self.storyboard!.instantiateViewControllerWithIdentifier(MainStoryboard.ControllerIdentifiers.Regulations) 
            }
            let navController = UINavigationController(rootViewController: detailController)
            splitController.showDetailViewController(navController, sender: self)
            
            
        }
    }
    
}

//    MARK: - Collection Veiw Datasource
extension ExtrasViewController {
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MainStoryboard.CellIdentifiers.mainCell, forIndexPath: indexPath) as! ExtraCollectionCell
        let item = self.dataSource[indexPath.row]
        cell.textLabel.text = item["title"]
        let imageName = item["image"]
        if let image = UIImage(named: imageName!) {
            cell.imageView.image = image
        } else {
            print("\(imageName)")
        }
        
        return cell
    }
}

//    MARK: - Collection View Flowlayout delegate
extension ExtrasViewController {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = isIPhone() ? CGRectGetWidth(collectionView.bounds) : CGRectGetWidth(collectionView.bounds) / 2.0
        return CGSizeMake(width - 1.0, 44.0)
    }
}

//   MARK: - Scrollview delegate
extension ExtrasViewController {
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        bannerView.center.x = CGRectGetMidX(scrollView.bounds)
        bannerView.center.y = scrollView.contentOffset.y + scrollView.bounds.size.height - CGRectGetMidY(bannerView.bounds)
    }
}
