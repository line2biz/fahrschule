//
//  QuestionSheetViewController.swift
//  Fahrschule
//
//  Created on 15.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit




let reuseIdentifier = "Cell"

class QuestionSheetViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,  UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
//    MARK: - Types
    struct MainStoryboard {
        struct SegueIdentifiers {
            static let showTestResults = "LearningResultViewController"
            static let showExamResults = "showExamResults"
            static let showOfficialResults = "ShowOfficialResults"
        }
    }
    
//    LearningResultViewController
    
//    MARK: Properties
    
    var managedObjectContext: NSManagedObjectContext!
    var currentIndex: Int = 0
    var currentIndexPath: NSIndexPath!
    var questionModels: [QuestionModel]!
    var questionSheetType: QuestionSheetType = .Learning
    var isOfficialLayout = NSUserDefaults.standardUserDefaults().boolForKey(SettingsOfficialModeKey)
    
//    var masterViewController: QuestionsTableViewController?
    private var masterNavigationController: UINavigationController?
    private var detailNavigationController: UINavigationController?
    
    // Observers
    private var localObservers = [AnyObject]()
    
    // Timer
    private var examTimer: NSTimer?
    var timeLeft: Int = 60 * 60
    
    
    var solutionIsShown: Bool = false
    
//    MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var prevBarButton: UIBarButtonItem!
    @IBOutlet weak var nextBarButton: UIBarButtonItem!
    @IBOutlet weak var titleBarButton: UIBarButtonItem!
    @IBOutlet var solutionButton: UIBarButtonItem!
    
    @IBOutlet var interruptButton: UIBarButtonItem!
    @IBOutlet var submitButton: UIBarButtonItem!
    
    @IBOutlet weak var timerLabel: UILabel!
    
//    MARK: - Initialization and delocation
    deinit {
        unregisterObservers()
    }
    
//    MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
//        Flow Layout
        var flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .Horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        collectionView?.pagingEnabled = true
        collectionView?.collectionViewLayout = flowLayout
        
//        Configure
        self.navigationController?.toolbarHidden = false
        configureView()

        
//         Register cell classes
        let nameSpaceClassName = NSStringFromClass(InquirerCollectionCell.self)
        let className = nameSpaceClassName.componentsSeparatedByString(".").last! as String
        collectionView!.registerNib(UINib(nibName: className, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
            
        
        
        
        
//        Navigation bar
        var barButtonTitle = ""
        if self.questionSheetType == .Exam && !self.solutionIsShown {
            
            startTimer()
            self.timerLabel.text = timeLeftString()
            
            self.navigationItem.leftBarButtonItem = self.interruptButton
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if let splitController = navigationController?.parentViewController as? UISplitViewController {
            if splitController.viewControllers.count > 1 {
                masterNavigationController = splitController.viewControllers.first as? UINavigationController
                detailNavigationController = splitController.viewControllers.last as? UINavigationController
            } else {
                print("Unexpected error: QuestionsTableViewController \(__FUNCTION__)")
            }
            
            
        }
        
//        Register observers
        registerObservers()
        

    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let flowLayout = self.collectionView?.collectionViewLayout
        flowLayout?.invalidateLayout()
        if isOfficialLayout && questionSheetType.contains(.Exam) {
            self.navigationController?.toolbarHidden = true
        } else {
            self.navigationController?.toolbarHidden = false
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let path =  currentIndexPath{
            showQuestionAtIndexPath(path)
            NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationDidSelectQuestion, object: path)
        }
        
        currentIndexPath = nil
        
    }
    
    internal override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.Portrait
        } else {
            return UIInterfaceOrientationMask.Landscape
        }
    }
    
    
    internal override func shouldAutorotate() -> Bool {
        return true
    }
    
//    MARK: - Observers
    func registerObservers() {
//        SettingsNotificationDidTagQuestionOfficial
        
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()

        
        localObservers.append(center.addObserverForName(SettingsNotificationHandInExamAndShowResult, object: nil, queue: queue, usingBlock: { [weak self] note -> Void in
            if let weakSelf = self {
                weakSelf.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showOfficialResults, sender: weakSelf)
            }
        }))
        
        
    }
    
    func unregisterObservers() {
        let center = NSNotificationCenter.defaultCenter()
        for observer in self.localObservers {
            center.removeObserver(observer)
        }
    }
    
//    MARK: - Public functions
    func showQuestionAtIndexPath(indexPath: NSIndexPath) {
        self.collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: false)
        self.configureView()
    }
    
    private func scrollToIndexPath(indexPath: NSIndexPath) {
        let count: CGFloat = CGFloat(indexPath.item)
        let width: CGFloat = CGRectGetWidth(self.collectionView!.bounds)
        let offsetX =  count * width // CGFloat(indexPath.item)*CGRectGetWidth(self.collectionView?.bounds)
        self.collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        let delay = 0.25 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue()) { _ in
            self.currentIndex = Int(round(self.collectionView!.contentOffset.x / CGRectGetWidth(self.collectionView!.bounds)))
            let indexPath = NSIndexPath(forRow: self.currentIndex, inSection: 0)
            
            if let path = self.currentIndexPath {
                if indexPath.compare(self.currentIndexPath) != .OrderedSame {
                    self.currentIndexPath = indexPath
                    NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationDidSelectQuestion, object: indexPath)
                }
            } else {
                self.currentIndexPath = indexPath
                NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationDidSelectQuestion, object: indexPath)
            }
            
            self.configureView()
        }
        
    }
    
    
//    MARK: - Private functions
    private func configureView() {
        
        self.currentIndex = Int(round(self.collectionView!.contentOffset.x / CGRectGetWidth(self.collectionView!.bounds)))
        
        let model: QuestionModel = self.questionModels[self.currentIndex]
        var title = "\(self.currentIndex + 1)/\(self.questionModels.count) |  \(model.question.points) Pkt."
        if model.question.number.hasSuffix("-M") {
            title += " | M"
        }
        self.navigationItem.title = title
        
        // Next & Prev buttons
        self.prevBarButton.enabled = !(self.currentIndex == 0)
        self.nextBarButton.enabled = (self.currentIndex + 1 < self.questionModels.count)
        
        // Solution button
        if let btn = self.solutionButton {
            btn.enabled = !model.hasSolutionBeenShown
        }
        
        // Remove 'Solution' or add 'Abgeben' button
        if (self.solutionIsShown) {
            self.navigationItem.leftBarButtonItem = nil
            self.solutionButton.enabled = true
            var toolbarItems = self.toolbarItems as [UIBarButtonItem]!
            if let idx = toolbarItems.indexOf(self.solutionButton) {
                toolbarItems.removeAtIndex(idx)
                self.toolbarItems = toolbarItems
            }
        }
        
        
        // Add Submit button
        if self.questionSheetType == .Exam {
            var toolbarItems = self.toolbarItems as [UIBarButtonItem]!
            if toolbarItems.contains(self.submitButton) == false {
                toolbarItems.insert(self.submitButton, atIndex: 2)
            }
            
            if toolbarItems.contains(self.solutionButton) {
                let idx = toolbarItems.indexOf(self.solutionButton)
                toolbarItems.removeAtIndex(idx!)
            }
            
            self.toolbarItems = toolbarItems
        }
        
        
    }
    
    func examTimeLeft(sender: AnyObject) {
        self.timeLeft--
        
        if self.timeLeft % 60 == 0 || self.timeLeft <= 60 {
            if self.timeLeft == 0 {
                handInExamAndShowResult()
            }
            else if self.timeLeft < 60 {
                self.timerLabel.textColor = UIColor.redColor()
            }
            
        }
        
        self.timerLabel.text = timeLeftString()
    }
    
    func timeLeftString()->String {
        let left = ceil(Double(self.timeLeft) / 60.0)
        return self.timeLeft <= 60 ? "\(self.timeLeft)" : "\(Int(left)) min";
    }
    
    func handInExamAndShowResult() {
        self.examTimer?.invalidate()
        self.examTimer = nil
        self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showExamResults, sender: self)
    }
    
    func startTimer() {
        if let timer = examTimer {
            timer.invalidate()
        }
        
        self.examTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("examTimeLeft:"), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        if let timer = examTimer {
            self.examTimer?.invalidate()
            self.examTimer = nil
        }
    }

    func numberOfAnsweredQuestions()->Int {
        var qty: Int = 0;
        for model in self.questionModels {
            if model.isAnAnswerGiven() {
                qty++;
            }
        }
        return qty
    }
    
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Check Timer
        stopTimer()
        if segue.destinationViewController is LearningResultViewController || segue.destinationViewController is ExamResultViewController {

            if let resultsVC = segue.destinationViewController as? LearningResultViewController {
                var index = 0
                var numQuestionsNotCorrectAnswered = 0
                var mutableArray = [QuestionModel]()
                var questions = [Question]()
                
                for model in questionModels {
                    if model.isAnAnswerGiven() {
                        let newModel = QuestionModel(question: model.question, atIndex: index++)
                        newModel.givenAnswers = model.givenAnswers
                        mutableArray.append(newModel)
                        questions.append(newModel.question)
                        
                        var arr = model.givenAnswers as [AnyObject]
                        if LearningStatistic.addStatistics(arr, forQuestion: model.question, inManagedObjectContext: self.managedObjectContext) == .FaultyAnswered {
                            numQuestionsNotCorrectAnswered++
                        }
                    }
                    
                    model.givenAnswers = nil
                    model.hasSolutionBeenShown = false
                    
                }
                
                resultsVC.managedObjectContext = managedObjectContext
                resultsVC.numQuestionsNotCorrectAnswered = numQuestionsNotCorrectAnswered
                resultsVC.questionModels = mutableArray
                
                if let questionsController = masterNavigationController?.topViewController as? QuestionsTableViewController {
                    questionsController.dataSource = mutableArray
                    if questionsController.questionSheetType.contains(.History) {
                        questionsController.questionSheetType = QuestionSheetType( questionsController.questionSheetType.rawValue + QuestionSheetType.History.rawValue )
                    }
                    
                }
                
                
            }
            else if let examResultVC = segue.destinationViewController as? ExamResultViewController {
                
                if let questionsController = masterNavigationController?.topViewController as? QuestionsTableViewController {
                    if questionsController.questionSheetType.contains(.History) {
                        questionsController.questionSheetType = QuestionSheetType( questionsController.questionSheetType.rawValue + QuestionSheetType.History.rawValue )
                    }
                    
                }
                
                examResultVC.managedObjectContext = managedObjectContext
                examResultVC.questionModels = questionModels
                examResultVC.timeLeft = timeLeft
            }
            
            SNAppDelegate.sharedDelegate().saveContext()
            
        }
        else if let controller = segue.destinationViewController as? OfficialResultViewController {
            controller.managedObjectContext = managedObjectContext
            controller.questionModels = questionModels
            controller.timeLeft = timeLeft
            SNAppDelegate.sharedDelegate().saveContext()
        }
        
    }
    
    // MARK: - Outlet Functions
    @IBAction func didTapButtonPrev(sender: AnyObject) {
        let idx = Int(round(self.collectionView!.contentOffset.x / CGRectGetWidth(self.collectionView!.bounds)))
        if idx > 0 {
            let indexPath = NSIndexPath(forItem: idx - 1, inSection: 0)
            self.scrollToIndexPath(indexPath)
        }
    }
    
    @IBAction func didTapButtonNext(sender: AnyObject?) {
        let idx = Int(round(self.collectionView!.contentOffset.x / CGRectGetWidth(self.collectionView!.bounds)))
        if idx < self.questionModels.count - 1 {
            let indexPath = NSIndexPath(forItem: idx + 1, inSection: 0)
            self.scrollToIndexPath(indexPath)
        }
    }
    
    func didTapButtonFavorites(sender: UIButton) {
        sender.selected = !sender.selected
        let center = self.collectionView!.convertPoint(sender.center, fromView: sender.superview) as CGPoint
        let indexPath = self.collectionView!.indexPathForItemAtPoint(center)
        let qModel: QuestionModel = self.questionModels[self.currentIndex]
        qModel.question.setTagged(sender.selected, inManagedObjectContext: self.managedObjectContext)
        NSNotificationCenter.defaultCenter().postNotificationName("tagQuestion", object: nil)
        
    }
    
    @IBAction func didTapButtonSolution(sender: UIBarButtonItem) {
//        sender.enabled = false
        let arr = self.collectionView!.indexPathsForVisibleItems()
        let indexPath = arr.first as NSIndexPath!
        let cell = self.collectionView?.cellForItemAtIndexPath(indexPath) as! InquirerCollectionCell!
        
        if cell.questionModel.isAnAnswerGiven() == true {
            var arr = cell.questionModel.givenAnswers as [AnyObject]
            LearningStatistic.addStatistics(arr, forQuestion: cell.questionModel.question, inManagedObjectContext: self.managedObjectContext)
        
        }
        
        self.solutionButton.enabled = false
        cell.questionModel.hasSolutionBeenShown = true
        cell.showSolutions()
        cell.tableView.allowsSelection = false

    }
    
    @IBAction func didTapButtonInterrupt(sender: AnyObject) {
        
        if self.questionSheetType == .Exam && !self.solutionIsShown {
            
            // Number of given answers
            var numAnsweredQuestions = numberOfAnsweredQuestions()
            
            
            //  No answers have been given
            if numAnsweredQuestions < 0 {
                self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
            } else {
                
                // Show alert
                let title = NSLocalizedString("Unterbrechen", comment: "")
                let msg = NSLocalizedString("Möchtest du die Prüfung wirklich unterbrechen?", comment: "")
                let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
                let yesBtn = NSLocalizedString("Ja",comment: "")
                let noBtn = NSLocalizedString("Nein", comment: "")
                
                
                // 'NO' action
                alertController.addAction(UIAlertAction(title: noBtn, style: .Cancel, handler: nil))
                
                // 'YES' action
                alertController.addAction(UIAlertAction(title: yesBtn, style: UIAlertActionStyle.Default, handler: { _ in
                    // Stop timer
                    self.stopTimer()
                    
                    // Caluclate statistic
                    let examStat = ExamStatistic.insertNewStatistics(self.managedObjectContext, state: .CanceledExam)
                    
                    if let stat = examStat {
                        
                        stat.index = self.currentIndex
                        stat.timeLeft = self.timeLeft
                        
                        for model in self.questionModels {
                            examStat.addStatisticsWithModel(model, inManagedObjectContext: self.managedObjectContext)
                        }
                        
                    }
                    
                    
                    
                    // Store data into database
                    SNAppDelegate.sharedDelegate().saveContext()
                    
                    // Dismiss controller & update badge number
                    self.dismissViewControllerAnimated(true, completion: { _ in
                        NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationUpdateBadgeValue, object: nil)
                    })

                }))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
        else if self.questionSheetType != .Exam && !Settings.sharedSettings().solutionMode {
            var givenAnswersCount = 0
            for qm in self.questionModels {
                if qm.isAnAnswerGiven() {
                    givenAnswersCount++
                }
            }
            
            if givenAnswersCount > 0 {
                self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showTestResults, sender: self)
            } else {
                self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
            }
            
        }

    }
    
    @IBAction func didTapButtonSubmit(sender: AnyObject) {
        // Number of given answers
        let numAnsweredQuestions = numberOfAnsweredQuestions()
        
        let answersToPass = self.questionModels.count - numAnsweredQuestions
        if answersToPass > 0 {

            let title = NSLocalizedString("Abgeben", comment: "")
            let msg = String.localizedStringWithFormat(NSLocalizedString("Du hast %d Fragen noch nicht beantwortet. Möchtest du trotzdem abgeben?", comment: ""), answersToPass)
            let yesBtn = NSLocalizedString("Ja",comment: "")
            let noBtn = NSLocalizedString("Nein", comment: "")
            
            let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)

            // 'NO' action
            alertController.addAction(UIAlertAction(title: noBtn, style: .Cancel, handler: nil))
            
            // 'YES' action
            alertController.addAction(UIAlertAction(title: yesBtn, style: .Default, handler: { _ in
                self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showExamResults, sender: self)
            }))

            self.presentViewController(alertController, animated: true, completion: nil)

        } else {
            self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showExamResults, sender: self)
        }
    }
    
    
    @IBAction func didTapButtonClose(sender: AnyObject) {
//        else if self.questionSheetType != .Exam && !Settings.sharedSettings().solutionMode {
        
        var givenAnswersCount = 0
        for qm in self.questionModels {
            if qm.isAnAnswerGiven() {
                givenAnswersCount++
            }
        }
        
        if questionSheetType.contains(.Learning)  {
            if givenAnswersCount > 0 {
                self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.showTestResults, sender: self)
            } else {
                
                if isIPAD() {
                    masterNavigationController?.popViewControllerAnimated(true)
                    navigationController?.popViewControllerAnimated(true)
                } else {
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
                
            }
            
        }
        
        

    }
    
    

    // MARK:  - Collection View datasource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.questionModels.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let model = self.questionModels[indexPath.item]

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! InquirerCollectionCell
        if self.solutionIsShown {
            model.hasSolutionBeenShown = true
        }
        cell.questionModel = model
        return cell
        
    }

//    MARK: - UICollectionView delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? InquirerCollectionCell {
            cell.configureView()
        }
    }
    
    
//    MARK: - Collection Flow layout delegate
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), CGRectGetHeight(collectionView.bounds) - collectionView.contentInset.top - collectionView.contentInset.bottom)
    }
    
//    MARK: - Scroll View delegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let lockQueue = dispatch_queue_create("com.Fahrschule.LockQueue", DISPATCH_QUEUE_SERIAL)
        dispatch_sync(lockQueue) {
            self.configureView()
            self.currentIndexPath = NSIndexPath(forItem: self.currentIndex, inSection: 0)
            self.currentIndex = Int(round(self.collectionView!.contentOffset.x / CGRectGetWidth(self.collectionView!.bounds)))
            
            NSNotificationCenter.defaultCenter().postNotificationName(SettingsNotificationDidSelectQuestion, object: self.currentIndexPath)
        }
    }
    
}

