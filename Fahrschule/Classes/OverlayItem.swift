//
//  OverlayItem.swift
//  Test
//
//  Created on 15.07.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

@IBDesignable

class OverlayItem: UIView {

//    MARK: - Types
//    MARK: Properties
    var text: String?
    
    var selected: Bool = false
    private var animated: Bool = false
    
    struct OverlayItemOptions {
        static let duration = 0.25
        static let increaseRate: CGFloat = 25.0
    }
    
//    MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        self.backgroundColor = UIColor.clearColor()
    }
    
//    MARK: - View Life cycle
    override func willMoveToSuperview(newSuperview: UIView?) {
        if CGSizeEqualToSize(frame.size, CGSizeZero) {
            if let overlayView = newSuperview as? OverlayView {
                let oldCenter = center
                frame.size = overlayView.overlayOptions.itemSize
                center = oldCenter
            }
        }
    }
    
//    MARK: - Public functions
    func setSelected(selected: Bool, animated: Bool) {
        
        self.selected = selected
        var increase: CGFloat = self.selected ? -1.0 : 1.0
        
        if selected {
            increase = increase * OverlayItemOptions.increaseRate / 2.0
        } else {
            increase = increase * OverlayItemOptions.increaseRate / 2.0
        }
        
        
        let finalFrame = CGRectInset(frame, increase,  increase)
        
        self.animated = true
        self.setNeedsDisplay()
        
        let overlayView = superview as? OverlayView
        overlayView?.holeRect = CGRectNull
        
        UIView.animateWithDuration(OverlayItemOptions.duration, animations: { _ in
            self.frame = finalFrame
        }) { finished in
            self.animated = false
            self.setNeedsDisplay()
            if self.selected {
                overlayView?.holeRect = finalFrame
            }
        }
        
    }
    
//    MARK: - Drawing
    override func drawRect(rect: CGRect) {
        if animated == false && selected == false {
            let ovalPath = UIBezierPath(ovalInRect: CGRectInset(bounds, 5, 5))
            UIColor.grayColor().setFill()
            ovalPath.fill()
            
            
            let font = UIFont.boldSystemFontOfSize(28)
            let str = NSAttributedString(string: "?", attributes: [ NSFontAttributeName: font ])
            var textRext = str.boundingRectWithSize(rect.size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, context: nil)
            textRext.origin.x = CGRectGetMidX(rect) - CGRectGetMidX(textRext)
            textRext.origin.y = CGRectGetMidY(rect) - CGRectGetMidY(textRext)
            str.drawInRect(textRext)
            
        }
        
        // Get the Graphics Context
        let context = UIGraphicsGetCurrentContext();
        
        // Set the circle outerline-width
        CGContextSetLineWidth(context, 5.0);
        
        // Set the circle outerline-colour
        UIColor.lemonColor().set()
        
        // Create Circle
        CGContextAddArc(context, (frame.size.width)/2, frame.size.height/2, (frame.size.width - 5)/2, 0.0, CGFloat(M_PI * 2.0), 1)
        
        // Draw
        CGContextStrokePath(context);
    }
    
}
