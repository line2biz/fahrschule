//
//  SubGroupTableViewController.swift
//  Fahrschule
//
//  Created on 19.06.15.
//  Copyright (c) 2015 RE'FLEKT. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SubGroupTableViewController: UIViewController {
    
//    MARK: - Types
    struct MainStoryboard {
        
        struct TableViewCellIdentifiers {
            static let subgroupCellIdentifier = "Cell"
        }
        
        struct Restoration {
            static let managedObjectID = "managedObjectID"
        }
        
        struct ViewControllerIdentifier {
            static let QuestionsList = "QuestionsTableViewController"
            static let Questions = "QuestionSheetViewController"
        }
    }
    
//    MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // Google banner
    @IBOutlet weak var bannerView: GADBannerView!
    
//    MARK: Properties
    private var masterNavigationController: UINavigationController?
    private var detailNavigationController: UINavigationController?
    var managedObjectContext: NSManagedObjectContext!
    var dataSource: [SubGroup]!
    var mainGroup: MainGroup! {
        didSet {
            self.dataSource = SubGroup.subGroupsInRelationsTo(self.mainGroup, inManagedObjectContext: self.managedObjectContext) as! [SubGroup]
        }
    }
    var selectedIndexPath: NSIndexPath? // Uses to select row when controller shown in master from detail controller
    
    
    
//    MARK: - State Save and Preservation
    override func encodeRestorableStateWithCoder(coder: NSCoder) {
        super.encodeRestorableStateWithCoder(coder)
        coder.encodeObject(self.mainGroup.objectID.URIRepresentation(), forKey: MainStoryboard.Restoration.managedObjectID)
    }
    
    override func decodeRestorableStateWithCoder(coder: NSCoder) {
        super.decodeRestorableStateWithCoder(coder)
        
        //        Restore Subgroup
        self.managedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext
        
        let objURI = coder.decodeObjectForKey(MainStoryboard.Restoration.managedObjectID) as! NSURL
        let objID: NSManagedObjectID = self.managedObjectContext.persistentStoreCoordinator!.managedObjectIDForURIRepresentation(objURI)!
        self.mainGroup = self.managedObjectContext.objectWithID(objID) as! MainGroup
        
        
        
    }
    
    override func applicationFinishedRestoringState() {
        self.title = self.mainGroup.name
    }

//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Table Settings
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Zurück", comment: ""), style: .Plain, target: nil, action: nil)
        
        if let path = selectedIndexPath {
            tableView.selectRowAtIndexPath(path, animated: false, scrollPosition: .None)
        }
        
        if let splitController = navigationController?.parentViewController as? UISplitViewController {
            if splitController.viewControllers.count > 1 {
                masterNavigationController = splitController.viewControllers.first as? UINavigationController
                detailNavigationController = splitController.viewControllers.last as? UINavigationController
            }
        }
        
        // Google banner
        bannerView.adUnitID = ApplicationGoogleAdsID
        bannerView.rootViewController = self;
        let request: GADRequest = GADRequest()
        request.testDevices = [ kGADSimulatorID, "d11daefd6bf3ab4ac67af8a6d8b8b264" ]
        bannerView.loadRequest(request)
        tableView.contentInset.bottom = CGRectGetHeight(bannerView.frame)

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(true, animated: animated)
    }


//    MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? QuestionsTableViewController {
            let indexPath: NSIndexPath = self.tableView!.indexPathForSelectedRow!
            vc.managedObjectContext = self.managedObjectContext
            vc.subGroup = self.dataSource[indexPath.row]
        }
        else if let models = sender as? [QuestionModel] {
            if let navController = segue.destinationViewController as? UINavigationController {
                if let qsvc = navController.topViewController as? QuestionSheetViewController {
                    qsvc.managedObjectContext = self.managedObjectContext
                    for model in models {
                        model.givenAnswers = nil
                        model.hasSolutionBeenShown = false
                    }
                    qsvc.questionModels = models
                                        
                }
            }
        }
    }
    
//    MARK: - Outlet functions
    @IBAction func didTapButtonQuery(sender: UIBarButtonItem) {

        let title = NSLocalizedString("Abfragen", comment: "")
        let msg = NSLocalizedString("Was soll abgefragt werden?", comment: "");
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        
        // Cancel
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Abbrechen", comment: ""), style: UIAlertActionStyle.Cancel, handler: nil))
        
        // All topics
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle Themenbereiche", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            let questions = Question.questionsInRelationsTo(self.mainGroup, inManagedObjectContext: self.managedObjectContext) as! [Question]
            self.openQuestionSheetController(alertAction.title!, questions: questions)
            
        }))
        
        
        // All incorrect answers
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle falsch Beantworteten", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            let questions = Question.questionsInRelationsTo(self.mainGroup, state: StatisticState.FaultyAnswered, inManagedObjectContext: self.managedObjectContext) as! [Question]
            self.openQuestionSheetController(alertAction.title!, questions: questions)
            
        }))
        
        // All unanswered questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Alle Unbeantworteten", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            let questions = Question.questionsInRelationsTo(self.mainGroup, state: StatisticState.StateLess, inManagedObjectContext: self.managedObjectContext) as! [Question]
            self.openQuestionSheetController(alertAction.title!, questions: questions)
            
        }))
        
        // False & Unanswered questions
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Falsch & Unbeantwortete", comment: ""), style: .Default, handler: { (alertAction) -> Void in
            var questions = Question.questionsInRelationsTo(self.mainGroup, state: StatisticState.FaultyAnswered, inManagedObjectContext: self.managedObjectContext) as! [Question]
            questions += Question.questionsInRelationsTo(self.mainGroup, state: StatisticState.StateLess, inManagedObjectContext: self.managedObjectContext) as! [Question]
            self.openQuestionSheetController(alertAction.title!, questions: questions)
        }))

        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    

    
//    MARK: - Private functions
    private func openQuestionSheetController(title: String, questions: [Question]) {
        
        if let models = QuestionModel.modelsForQuestions(questions) as? [QuestionModel] {
            if models.count > 0 {
                // Detail controller
                let questionarieController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.Questions) as! QuestionSheetViewController
                questionarieController.managedObjectContext = managedObjectContext
                questionarieController.questionModels = models
                questionarieController.currentIndexPath = NSIndexPath(forRow: 0, inSection: 0)
                
                if isIPAD() {
                    // Master controller
                    let questionsController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.QuestionsList) as! QuestionsTableViewController
                    questionsController.title = title
                    questionsController.dataSource = models
                    questionsController.managedObjectContext = managedObjectContext
                    questionsController.questionSheetType = QuestionSheetType.Learning
                    
                    
                    masterNavigationController?.pushViewController(questionsController, animated: true)
                    detailNavigationController?.pushViewController(questionarieController, animated: true)
                    
                } else {
                    let navController = UINavigationController(rootViewController: questionarieController)
                    presentViewController(navController, animated: true, completion: nil)
                }
                
                
                
            }
        }
    }

}

//    MARK: - Table view data source
extension SubGroupTableViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(MainStoryboard.TableViewCellIdentifiers.subgroupCellIdentifier, forIndexPath: indexPath) as! SubgroupCell
        let subGroup = self.dataSource[indexPath.row]
        cell.numberLabel.text = subGroup.number
        cell.titleLabel.text =  subGroup.name
        return cell
    }
    
}

//    MARK: - Table View delegate
extension SubGroupTableViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let subGroup = dataSource[indexPath.row]
        let questionsController = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboard.ViewControllerIdentifier.QuestionsList) as! QuestionsTableViewController
        questionsController.managedObjectContext = managedObjectContext
        questionsController.subGroup = subGroup
        
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Pad:
            // Update master & detail controller
            
            if navigationController == masterNavigationController {
                // Update detail controller
                if let questionsController = detailNavigationController?.topViewController as? QuestionsTableViewController {
                    questionsController.subGroup = subGroup
                    questionsController.tableView.reloadData()
                    questionsController.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Top, animated: true)
                }
                
            } else {
                // Update master controller
                if masterNavigationController?.viewControllers.count > 2 {
                    masterNavigationController?.popToViewController(masterNavigationController!.viewControllers[2] , animated: false)
                }
                
                let subgroupController = storyboard?.instantiateViewControllerWithIdentifier("SubGroupTableViewController") as! SubGroupTableViewController
                subgroupController.managedObjectContext = managedObjectContext
                subgroupController.dataSource = dataSource
                subgroupController.title = self.title
                subgroupController.mainGroup = mainGroup
                subgroupController.masterNavigationController = masterNavigationController
                subgroupController.detailNavigationController = navigationController
                subgroupController.selectedIndexPath = tableView.indexPathForSelectedRow
                masterNavigationController?.pushViewController(subgroupController, animated: true)
                
                
                // Update detail controller
                questionsController.navigationItem.hidesBackButton = true
                navigationController?.pushViewController(questionsController, animated: true)
            }
            
        case .Phone:
            navigationController?.pushViewController(questionsController, animated: true)
        default: return
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
