//
//  ExamViewController.swift
//  Fahrschule
//
//  Created on 25.06.15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ExamViewController: UIViewController {
//    MARK: - Types
    struct MainStoryboard {
        struct SegueIdentifiers {
            static let StartExam = "StartExam"
            static let StartOfficialExam = "StartOfficialExam"
        }
    }
//    MARK: Properties
    var managedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext!
    var localObservers = [AnyObject]()
    var interstitial: GADInterstitial?
    

//    MARK: - Initialization & Delocation
    deinit {
        unregisterObservers()
    }
    
//    MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObservers()
        updateBadgeNumber()
        
        
        // Google AdMob
        interstitial = createAndLoadInterstitial()
        interstitial?.delegate = self
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let banner = interstitial {
            if banner.isReady {
                banner.presentFromRootViewController(self)
                interstitial = nil
            }
        }
        
    }
    
    
//    MARK: - Observers
    func registerObservers() {
        
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()
        localObservers.append(center.addObserverForName(SettingsNotificationUpdateBadgeValue, object: nil, queue: queue, usingBlock: { [weak self] _ in
            if let strongSelf = self {
                strongSelf.updateBadgeNumber()
            }
        }))
        
    }
    
    func unregisterObservers() {
        let center = NSNotificationCenter.defaultCenter()
        for observer in self.localObservers {
            center.removeObserver(observer)
        }
    }
    
//    Private functions
    private func updateBadgeNumber() {
        var badgerValue: String? = nil
        if ExamStatistic.statisticsInManagedObjectContext(self.managedObjectContext, fetchLimit: -1, state: .CanceledExam).count > 0 {
            badgerValue = "1"
        }
        self.navigationController!.tabBarItem.badgeValue = badgerValue
    }
    
    

//    MARK: - Navigation
    @IBAction func exitToExamViewController(segue: UIStoryboardSegue) { /* Storyboard Exit */ }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let splitController = segue.destinationViewController as? UISplitViewController {
            let navigationController = splitController.viewControllers.first as? UINavigationController
            let masterController = navigationController?.topViewController as! QuestionsTableViewController
            
            let secondaryNavigationController = splitController.viewControllers.last as! UINavigationController
            let detailController = secondaryNavigationController.topViewController as! QuestionSheetViewController
            
            masterController.detailNavigationController = secondaryNavigationController
//            if isIPAD() {
//                detailController.masterViewController = masterController
//            }
            
            // Generate questions
            let models: [QuestionModel]
            if let examStat = ExamStatistic.statisticsInManagedObjectContext(managedObjectContext, fetchLimit: -1, state: .CanceledExam).last as? ExamStatistic {
                masterController.currentIndexPath = NSIndexPath(forRow: examStat.index.integerValue, inSection: 0)
                detailController.timeLeft = examStat.timeLeft.integerValue
                models = examStat.questionModelsForExam() as! [QuestionModel]
            } else {
                let questions = Question.examQuestionsInManagedObjectContext(managedObjectContext)
                models = QuestionModel.modelsForQuestions(questions) as! [QuestionModel]
                for model in models {
                    model.givenAnswers = nil
                    model.hasSolutionBeenShown = false
                }
                masterController.currentIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            }
            
            detailController.currentIndexPath = masterController.currentIndexPath!
            
            masterController.title = NSLocalizedString("Fragenübersicht", comment: "")
            masterController.navigationItem.rightBarButtonItem = nil
            masterController.managedObjectContext = managedObjectContext

            masterController.dataSource = models
            masterController.questionSheetType = .Exam
            
            
            detailController.managedObjectContext = self.managedObjectContext;
            detailController.questionModels = models
            
            detailController.questionSheetType = .Exam
            detailController.title = NSLocalizedString("Prüfung", comment: "")
            splitController.delegate = masterController

        }
        
        
    }
    
//    MARK: - Outlet functions
    @IBAction func didTapButtonStart(sender: AnyObject) {
        
        if let examStat = ExamStatistic.statisticsInManagedObjectContext(managedObjectContext, fetchLimit: -1, state: .CanceledExam).last as? ExamStatistic {
            let msg = NSLocalizedString("Möchtest du die letzte Prüfung fortsetzen?", comment: "")
            let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .Alert)
           
            // Cancel
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Abbrechen", comment: ""), style: .Cancel, handler: nil))
            
            // Continue old
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Prüfung fortsetzen", comment: ""), style: .Default, handler: { _ in
                self.startExam()
            }))
            
            // Start new
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Neue Prüfung starten", comment: ""), style: .Default, handler: { _ in
                // Delete previous exam stat
                examStat.deleteExamInManagedObjectContext(self.managedObjectContext)
                self.updateBadgeNumber()
                
                // show exam sheet
                self.startExam()
            }))
            
            
            // Show alert controller
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            // show exam sheet
            startExam()
        }
    }
    
//    MARK: - Private functions
    func startExam () {
        if NSUserDefaults.standardUserDefaults().boolForKey(SettingsOfficialModeKey) == true {
            
            let storyboard = UIStoryboard(name: "Official", bundle: nil)
            let containerController = storyboard.instantiateInitialViewController() as! OfficialContainerController
            
            let params = generateQuesionsForExam()
            
            containerController.managedObjectContext = managedObjectContext
            containerController.questionModels = params.models
            containerController.currentIndexPath = params.indexPath
            containerController.timeLeft = params.timeLeft
            
            presentViewController(containerController, animated: true, completion: nil)
            
        } else {
            self.performSegueWithIdentifier(MainStoryboard.SegueIdentifiers.StartExam, sender: self)
        }
        
    }
    
/*
    Generates questions if last exam was iterrupted returns last exam, current questions and time
*/
    func generateQuesionsForExam()->(models: [QuestionModel], indexPath: NSIndexPath, timeLeft: Int) {
        var models: [QuestionModel]
        var indexPath: NSIndexPath
        var timeLeft: Int = 60 * 60
        
        // Generate questions
        if let examStat = ExamStatistic.statisticsInManagedObjectContext(managedObjectContext, fetchLimit: -1, state: .CanceledExam).last as? ExamStatistic {
            models = examStat.questionModelsForExam() as! [QuestionModel]
            indexPath = NSIndexPath(forRow: examStat.index.integerValue, inSection: 0)
            timeLeft = examStat.timeLeft.integerValue
            
        } else {
            let questions = Question.examQuestionsInManagedObjectContext(managedObjectContext)
            models = QuestionModel.modelsForQuestions(questions) as! [QuestionModel]
            for model in models {
                model.givenAnswers = nil
                model.hasSolutionBeenShown = false
            }
            indexPath = NSIndexPath(forRow: 0, inSection: 0)
            timeLeft = 60 * 60
        }
        
        return (models, indexPath, timeLeft)
    }
    
    func log(logMessage: String, className: String = __FILE__,  functionName: String = __FUNCTION__) {
        print("\(className) \(functionName): \(logMessage)")
    }
    
    func DLog(message: String, file: String = __FILE__, function: String = __FUNCTION__, line: Int = __LINE__, column: Int = __COLUMN__) {
        let fileName = file.componentsSeparatedByString("/").last
        print("\(fileName) : \(function) : line \(line) - \(message)")
    }
}

// MARK: - Google AdMob delegate
extension ExamViewController: GADInterstitialDelegate {
    func interstitial(ad: GADInterstitial!, didFailToReceiveAdWithError error: GADRequestError!) {
        print("ExamViewController : \(__FUNCTION__) error: \(error.localizedDescription)")
    }
    
    func interstitialDidDismissScreen(ad: GADInterstitial!) {}
    
    func interstitialDidReceiveAd(ad: GADInterstitial!) {
        if let banner = interstitial {
            if banner.isReady {
                banner.presentFromRootViewController(self)
                interstitial = nil
            }
        }
    }
    
    func interstitialWillPresentScreen(ad: GADInterstitial!) {}
}
