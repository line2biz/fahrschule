//
//  OfficialContainerController.swift
//  Fahrschule
//

import UIKit

class OfficialContainerController: UIViewController {
    
    var managedObjectContext: NSManagedObjectContext = SNAppDelegate.sharedDelegate().managedObjectContext
    var questionModels: [QuestionModel]!
    var questionSheetType: QuestionSheetType = .Exam
    var currentIndexPath: NSIndexPath!
    var timeLeft: Int = 0

    //    MARK: - Rotation
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let value = UIInterfaceOrientation.LandscapeLeft.rawValue
//        UIDevice.currentDevice().setValue(value, forKey: "orientation")
//    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Landscape
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailController = (segue.destinationViewController as? UINavigationController)!.topViewController as? OfficialQuestionsController {
            detailController.managedObjectContext = managedObjectContext;
            detailController.questionModels = questionModels
            detailController.questionSheetType = .Exam
            detailController.currentIndexPath = currentIndexPath
            detailController.timeLeft = timeLeft
        }
    }

}
