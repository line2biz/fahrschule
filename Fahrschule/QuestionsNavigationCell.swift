//
//  CatalogCollectionCell.swift
//  Fahrschule
//


import UIKit

//class CatalogCollectionCell: UICollectionViewCell {

class QuestionsNavigationCell: UICollectionViewCell {

//    MARK: - Types
//    MARK: Outlets
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var answered: Bool = false { didSet { configureView() } }
    var tagged: Bool = false { didSet { configureView() } }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        answered = false
        
    }
    
    func configureView() {
        
        switch answered {
        case false where tagged == false:
            imageView.image = UIImage(named: "question_btn_normal")
            imageView.highlightedImage = UIImage(named: "question_btn_frame_normal")
            
        case false where tagged == true:
            imageView.image = UIImage(named: "question_btn_normal_tagged")
            imageView.highlightedImage = UIImage(named: "question_btn_frame_normal_tagged")
            
        case true where tagged == false:
            imageView.image = UIImage(named: "question_btn_selected")
            imageView.highlightedImage = UIImage(named: "question_btn_frame_selected")
            
        case true where tagged == true:
            imageView.image = UIImage(named: "question_btn_selected_tagged")
            imageView.highlightedImage = UIImage(named: "question_btn_frame_selected_tagged")
            
        default: break
        }
    }
    
    

}
